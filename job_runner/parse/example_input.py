"""
Store the example input file
"""
example_input = """[input_type]
# set to False if run is single end
paired_end = True

[project]
# define a project name, run name and report name
# make sure to specify different name within a project/run
# or else the server will refuse to overwrite it 
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'

[trimming_galore]
# you should use [trimming_cutadapt] if you prefer cutadapt for trimming
# only trim_galore executable makes sense, add the path if not in $PATH
cmd = trim_galore
# you can define the cmdline options here below
# if you'd like to use the short version (e.g. -q 10 instead of 
# --quality 10), just set short_options = True 
quality = 10
stringency = 8
# if its a flag, set the name to True 
gzip = True
# depends None means that it will be the first one to execute along 
# along the dependency path
depends = None
# this means we will execute the command at file level
dependency_type = file
# uncomment next line if the folder produced should be removed when *all* jobs finalize
# clean_up = True

[align_bwa_sort]
# right now only bwa mem makes here
cmd = bwa mem
# put your reference genome here
reference = genome.fa
# bwa mem only uses short option arguments
short_options = True
M = True
t = 8
depends = trimming_galore
dependency_type = file
#
# uncomment next line if the folder produced should be removed when *all* jobs finalize
# clean_up = True
#
# you can add a memory keyword to alter the memory requested to the queueing system
# for this particular stage. This can be added to other stages as well.
# memory = 100G

[mergebam]
# this should stay like that 
cmd = samtools merge
depends = align_bwa_sort
dependency_type = folder
# uncomment next line if the folder produced should be removed when *all* jobs finalize
# clean_up = True

[preseq]
# this should stay like that
cmd = preseq
depends = mergebam
dependency_type = folder
# you can add the can_fail option to indicate that this job can fail and 
# won't kill the rest of the pipeline (unless it has dependencies(!!) as they
# will be stopped by the queueing system, and unless they also have a can_fail
# set to true, they will make the whole pipeline stop.
can_fail = True

[filterduplicates]
# this should stay like that
cmd = picard-2.8.2.jar
# set your white list here, required 
whitelist = mm9-whitelist.bed
# set the sam flags you'd want to use in the filter step
# these are the default values anyway, so you could remove the two lines
sam_include = 3
sam_exclude = 3840
depends = mergebam
dependency_type = folder

# you can also just filter, e.g.:
# [filter]
# cmd = picard-2.8.2.jar
# and add the cmd line options you'd want in it as pairs of key word = value
# F = 3840
# f = 3
# short_options = True
# depends = mergebam
# dependency_type = folder
# NB: make sure you've marked duplicates if you want to actually filter them
# see documentation

[chipseq]
cmd = macs2 callpeak
paired_control = no
control = _Input
enrich = _ChIP*
multifolder_postfix = _Input _ChIP*
depends = filterduplicates
dependency_type = multifolder

[quality]
# this should stay like that
cmd = fastqc
depends = trimming_galore
dependency_type = folder
# uncomment next line if the folder produced should be removed when *all* jobs finalize
# clean_up = True

[samstats]
# this should stay like that
cmd = samtools stats
depends = filterduplicates
dependency_type = folder
# see comment on [preseq] about can_fail and its implications
can_fail = True

[multiqc]
# this should stay like that
cmd = multiqc
# you can add multiple dependencies, separated by spaces
depends = quality preseq samstats
dependency_type = project
# uncomment next line if the folder produced should be removed when *all* jobs finalize
# clean_up = True

[submit]
# this should stay like that
cmd = submit_report.py
depends = multiqc
dependency_type = project

[folder_names]
control_rep_2 = control_rep_2

[extension_regex]
regex = _L???_R?_???.fastq.gz
no_lane = _R?_???.fastq.gz
# don't forget the dot before the extension
extension = .fastq.gz
# specifiers for read one and read two. If you have a special 
# separator before the read indicator, eg _, please include it
read_one = _R1_001
read_two = _R2_001
"""
