""" Parse the input file and make sure that all jobs are properly specified.

Here we define with jobs/stages are allowed in the input file, as well as
which sections (besides the stages) are allowed.



TODO:
    make sure all the jobs have dependencies specified
    remove hard-coded required options for stages??

"""
import configparser
import logging
import sys
from typing import Dict, List

from job_runner.planner.jobs import registered_jobs

logger = logging.getLogger("runner_logger")

# these we read from planner/jobs
known_stages = [job for job in registered_jobs]

known_sections = [
    "input_type",
    "folder_names",
    "extension_regex",
    "project",
]

# add the known stages here
known_sections.extend(known_stages)

# these inputs can not be specified simultaneously
EXCLUSIVE_INPUT = [{('short_options', 'keyvalue_options')}]


# maybe add something like this in the future, to make optional defaults
# easier to handle
# OPTIONAL = {'can_fail': False, 'memory': '60G'}]


def parse_option(
        required: List = None,
        optional: List = None, *,
        config: configparser.ConfigParser, field: str,
        constrain_dependency: str = None,
) -> Dict:
    """Parse the stages option.

    Stop the execution if we don't find a required option, or if
    two exclusive options are set simultaneously.

    We also add the optional entries to all the known stages. If it's has
    not been explicitly defined, we input here default values.
    can_fail defaults to False.
    memory defaults to 60G

    """
    input_dict = {}
    if not config.has_section(field):
        msg = f"Could not find compulsory [{field}] section in input file."
        logger.error(msg)
        sys.exit()
    else:
        for k, v in config.items(field):
            input_dict[k] = v

    # unless specified, let's set a couple of defaults
    # which only make sense if they refer to a [stage]
    # the "can_fail" and "memory" options should not be considered as
    # "keyword options" for the stage, so in jobs.py/SetupJob class
    # we should add them as not a kw_option.
    # Tests adapted to include the 'can_fail', and 'memory'
    # If the we see we want to add more defaults, we should consider
    # another more general strategy than adding them by "hand" here.
    # perhaps a dictionary with the defaults would be simple and work
    if field in known_stages:
        if "can_fail" not in input_dict:
            input_dict['can_fail'] = 'False'
        if "memory" not in input_dict:
            input_dict['memory'] = '60G'

    # make sure we don't have exclusive options
    options = {k for k in input_dict}

    c = 0
    for ex in EXCLUSIVE_INPUT:
        for x in ex:
            if x in options:
                c += 1
        if c > 1:
            msg = f"You have conflicting input values.\n" \
                f"These inputs can not be set simultaneously :" \
                f"{EXCLUSIVE_INPUT}"
            logger.error(msg)
            sys.exit()

    if required is not None:
        for req in required:
            if req not in input_dict:
                msg = f"Could not find compulsory option {req} " \
                    f"in input file section {field}."
                logger.error(msg)
                sys.exit()
    if constrain_dependency is not None:
        if input_dict['dependency_type'] != constrain_dependency:
            msg = f"The section {field} must have a dependency_type" \
                f" {constrain_dependency}"
            logger.error(msg)
            sys.exit()

    return input_dict


def find_stages(config) -> List:
    """ Return a list with all stages.

    Warn and stop is an section has not been identified
    """

    stages = []
    for section in config.sections():
        if section not in known_sections:
            msg = f"I've found a section I don't know about: {section} "
            logger.error(msg)
            sys.exit()
        elif section in known_stages:
            stages.append(section)

    return stages


def parse_regex(config):
    if config.has_option("extension_regex", "regex"):
        return config.get("extension_regex", "regex")
    else:
        logger.error(
            "You must specify [extension_regex] option "
            "with a regex entry, eg:"
            "[extension_regex]"
            "regex =  _L???_??_???.fastq.gz"
            "where name will be substituted by the folder name."
        )
        sys.exit()


def parse_input_file(inp_f: str, verbose: bool):
    """Read in input file.

    Reads the input file supplied by the user and returns a dict
    with the name of run as a key, and the SRR code as value.

    At the moment has limited input validation... fingers crossed.
    """
    config = configparser.ConfigParser()
    config.optionxform = str  # do not change to lowercase, it would by default
    config.read(inp_f)
    logger.debug("Reading configuration file")

    # check if we have things defined in Config section, otherwise use defaults
    if config.has_option("input_type", "paired_end"):
        if (
                config.get("input_type", "paired_end") == "True"
                or config.get("input_type", "paired_end") == "true"
        ):
            b_paired = True
        else:
            logger.debug("using options for single end reads.")
            b_paired = False
    else:
        logger.warning(
            "Assuming paired-end.... proceed at your own risk if this is wrong"
        )
        b_paired = True

    input_folder = parse_option(config=config, field="folder_names")

    if b_paired:
        regex = parse_option(config=config,
                             field="extension_regex",
                             required=[
                                 "regex",
                                 "no_lane",
                                 "extension",
                                 "read_one",
                                 "read_two",
                             ]
                             )
    else:
        regex = parse_option(config=config,
                             field="extension_regex",
                             required=[
                                 "regex",
                                 "no_lane",
                                 "extension",
                                 "read_one",
                             ]
                             )

    project = parse_option(config=config,
                           field="project",
                           required=[
                               "run_name",
                               "project_name",
                               "report_name",
                           ],
                           )

    stages = find_stages(config)
    dict_stages = {}
    for st in stages:
        if st == "align":
            dict_stages[st] = parse_option(config=config,
                                           field=st,
                                           required=[
                                               "cmd",
                                               "reference",
                                               "depends",
                                               "dependency_type",
                                           ]
                                           )
        elif st == "merge":
            dict_stages[st] = parse_option(config=config,
                                           field=st,
                                           required=[
                                               "cmd",
                                               "depends",
                                               "dependency_type",
                                           ]
                                           )

        elif st == "filterduplicates":
            dict_stages[st] = parse_option(config=config,
                                           field=st,
                                           required=[
                                               "cmd",
                                               "whitelist",
                                               "depends",
                                               "dependency_type",
                                           ],
                                           )
        elif st == "chipseq":
            dict_stages[st] = parse_option(config=config,
                                           field=st,
                                           required=[
                                               "cmd",
                                               "paired_control",
                                               "multifolder_postfix",
                                               "control",
                                               "enrich",
                                               "depends",
                                               "dependency_type",
                                           ],
                                           constrain_dependency="multifolder"
                                           )
        else:
            dict_stages[st] = parse_option(config=config,
                                           field=st,
                                           required=[
                                               "cmd",
                                               "depends",
                                               "dependency_type",
                                           ],
                                           )

    dictionary = {
        "project": project,
        "b_paired": b_paired,
        "input_folders": input_folder,
        "stages": dict_stages,
        "regex": regex,
    }
    return dictionary
