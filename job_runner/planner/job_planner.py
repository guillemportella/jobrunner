""" Establish the order for job execution and store job ids

Node, DependencyTree:

Classes to plan the execution order of the jobs to be performed based on
the dependencies they have. The DependencyTree get_plan method reads
in the dictionary of jobs/stages, builds a dependency tree and resolves
the path of execution in order to satisfy the dependencies. It returns a
dictionary with four keys: prepare, file, folder or project. In each we
list the order of the jobs that need to be executed.

The first job to be executed must depend on None, and it is the root of the
tree. We establish the order of executions by doing a depth-first search across
the tree.

StoreIds:

Keep track of the job ids of each stage/job. Before a job needs to be executed
check the job ids your stage/job depends on with get_ids. After the job
has returned an queue id, or a list of them, add them to the instance using
add_id().
Internally, the class stores/retrieves the ids in the right dictionary based
on what the arguments that are passed.
"""
import logging
import sys
from collections import deque
from typing import Union, List, Dict

logger = logging.getLogger("runner_logger")


class Node(object):
    """Store the node name, its type and its childs."""

    def __init__(self, name: str, fth: "List[Node]", attr: str):
        self.name = name
        self.father = fth
        self.style = attr
        self.dep = []

    def add(self, nd: "Node"):
        self.dep.append(nd)

    def __repr__(self):
        text = f"{self.name} ({self.style})"
        return text


STAGE_LEVELS = ['project', 'multifolder', 'folder', 'file', 'prepare']


class DependencyTree(object):
    """Tree holding the job dependencies.

    Required to create an execution plan in the order that
    fulfills the dependencies of each job.

    get_plan --> returns the execution plan as Dict containing list of jobs
                 keys are the dependency type of 'level' of that step

    get_father -> return the name of the job that depends on a given job

    """

    def __init__(self):
        self.node_dict = {}
        # must get the elements in reverse order
        # after a depth-first search of the tree
        self._path = deque()
        self._seen = []
        self.exec_plan = {level: [] for level in STAGE_LEVELS}

    def get_father(self, st: str) -> List[Node]:
        """Helper function to get the father of a particular task"""
        return self.node_dict[st].father

    def get_plan(self, *, stages_options: Dict) -> Dict[str, List]:
        """Build the dependency graph.

        :param stages_options: dict['stages']
        """
        for prc in stages_options:
            dp = stages_options[prc]['depends']
            st = stages_options[prc]['dependency_type']
            # Important: we have to build the tree backwards
            self._add_to_tree(dp, prc, st)

        return self._resolve()

    def _add_to_tree(self, a: str, b: str, st: str):
        """
        :param a: dependencies
        :param b: the stage
        :param st: dependency type
        """
        n_b = self._get_or_create(b, st)
        # we might have more than one depenency
        # for dep in a.replace(" ", "").split(","):
        for dep in a.split():
            n_a = self._get_or_create(dep, st)
            # could have more than one dependency
            # TODO : we have to check if that dependency is actually allowed!!
            # we now know who the father is...
            n_b.father.append(n_a)
            n_a.add(n_b)

    def _get_or_create(self, a: str, st: str):
        """Return a Node, create if it does not exist"""
        if a in self.node_dict:
            return self.node_dict[a]
        else:
            new_node = Node(a, [], st)
            self.node_dict[a] = new_node
            return new_node

    def _get_head(self, key="None"):
        try:
            x = self.node_dict[key]
        except KeyError as e:
            logger.error("Error, could not find head of dependency graph")
            logger.error("At least one process should depends on 'None'")
            sys.exit()

        if len(x.dep) == 1:
            return x.dep[0]
        else:
            logger.error(x)
            logger.error("Found more than one entry which depends on 'None'")
            logger.error("This is not supported, split into more jobs.")
            sys.exit()

    def _iterate(self, node):
        """Recursive exploration of graph, depth-first"""

        # this helps tracking circular deps https://goo.gl/3atw6b
        self._seen.append(node)
        for sub_node in node.dep:
            if sub_node not in self._path:
                if sub_node in self._seen:
                    exc_text = f"Circular reference detected: " \
                        f"{node.name} -> {sub_node.name}"
                    raise Exception(exc_text)
                else:
                    self._iterate(sub_node)
        self._path.appendleft(node)

    def _resolve(self) -> Dict[str, List]:
        """Resolve order of dependencies, starting at 'None'.

        Right now there must be one and only one entry with 'None'.
        Catch this during parsing, but would be nice to add some checks
        here just in case.
        """
        h = self._get_head(key="None")
        self._iterate(h)
        for nd in self._path:
            self.exec_plan[nd.style].append(nd.name)
        return self.exec_plan

    def __repr__(self):
        text = f"Tree with {len(self.node_dict)} nodes.\n" \
            f"{self.node_dict}"
        return text


class StoreIds(object):
    """Keep track and recall ids of processes.

    Used to keep the ids of jobs submitted to the cluster.
    add_id(): store the queue ids of a given job
    get_ids: return the queue ids that a job depends on
    """

    def __init__(self):
        self.prepare_id = {}
        self.file_id = {}
        self.folder_id = {}
        self.multifolder_id = {}
        self.project_id = {}
        self.id_can_fail = {}  # here we can keep a map to can_fail bool

    def __repr__(self):
        text = f"Project ID: {self.project_id}\n" \
            f"Multifolder ID: {self.multifolder_id}\n" \
            f"Folder ID: {self.folder_id}\n" \
            f"File ID: {self.file_id}\n " \
            f"Prepare ID: {self.prepare_id}"
        return text

    def add_fail(self, job_id: int, st: str, run_options: Dict) -> bool:
        """Add ids to the dictionary of id_can_fail with values from input.

        We will need the map of ids to can_fail bools to decide if we kill
        all the jobs if one given job fails.
        """
        # check that we do have the can_fail key
        stage_options = run_options['stages'][st]
        logger.debug(f"Adding job_id {job_id} to dictionary of can_fail")
        try:
            can_fail = stage_options['can_fail']
        except KeyError as e:
            logger.error(f"Can not find option 'can_fail' in stage {st}")
            sys.exit()
        else:
            if can_fail == "False" or can_fail == "false":  # it's a str!
                self.id_can_fail[job_id] = False
                logger.debug(f"Setting {job_id} can_fail to False")
            elif can_fail == "True" or can_fail == "true":
                self.id_can_fail[job_id] = True
                logger.debug(f"Setting {job_id} can_fail to True")
            else:
                msg = (f"Option 'can_fail' in stage {st} should be either "
                       f"True or False, and not {can_fail}.")
                logger.error(msg)
                sys.exit()
            return True

    def add_id(self, fdr: str = None, fi: str = None, st: str = None, *,
               job_id: Union[int, None] = None,
               level: str = None) -> bool:
        """Determine the right dictionary based on input.

        :param level: which level are we in, one of STAGE_LEVELS
        :param st: stage name
        :param job_id: id of a job
        :param fdr: folder name
        :param fi:  file name
        :return: bool or exit

        """
        if job_id is None:
            logger.error(
                "'job_id' keyword must be defined in method .add")
            sys.exit()
        if st is None:
            logger.error("'st' keyword must be defined in method .add")
            sys.exit()

        if level not in STAGE_LEVELS:
            logger.error(f"You defined level {level}, which is not allowed,"
                         f" pick one:")
            logger.error(f"{STAGE_LEVELS}")
            sys.exit()

        # one of these cases in which pattern matching would help
        # if fi is None and fdr is None and level == 'project':
        if level == 'project':
            if fi is not None and fdr is not None:
                logger.error("Can not set file or folder at project level")
                sys.exit()
            else:
                self.project_id.setdefault(st, []).append(job_id)
                return True

        # if fi is None and fdr is None and level == '':
        if level == 'prepare':
            if fi is not None and fdr is not None:
                logger.error("Can not set file or folder at prepare level")
                sys.exit()
            else:
                self.prepare_id[st] = job_id
                logger.debug(f"Stored prepare {st}: jobid {job_id}")
                return True

        if level == "multifolder":
            if fi is not None:
                logger.error("You set a file name at multifolder level")
                sys.exit()
            else:
                self.multifolder_id.setdefault(fdr, {})
                self.multifolder_id[fdr].setdefault(st, job_id)
                return True

        if level == "folder":
            if fi is not None:
                logger.error("You set a file name at folder level")
                sys.exit()
            else:
                self.folder_id.setdefault(fdr, {})
                self.folder_id[fdr].setdefault(st, job_id)
                return True

        # if we made it here, it must be file_id dictionary
        if level == 'file':
            self.file_id.setdefault(fdr, {})
            self.file_id[fdr].setdefault(fi, {})
            self.file_id[fdr][fi].setdefault(st, job_id)

        return True

    def get_depending_ids(self, fdr: str = None, fi: str = None, *,
                          mfdr: List[str] = None,
                          fathers: List[Node] = None, level: str) -> List[int]:
        """

        :param level: which level are we in, one of STAGE_LEVELS
        :param fdr: folder name
        :para mfdr: list of folder names
        :param fi:  file name
        :param fathers:  List of Node object of father node
        :return: bool or exit

        """

        if fathers is None:
            logger.error(
                "Error, 'father' keyword must be defined in method .get_ids")
            sys.exit()

        ids = []
        for father in fathers:
            if level == 'prepare':
                if father is not None and father.name != "None":
                    if fi is None and fdr is None:
                        if father.style == "folder":
                            # aggregate all the ids from folders
                            ids.extend([self.folder_id[f][father.name] for f in
                                        self.folder_id])
                        else:
                            ids.extend([self.prepare_id[father.name]])
                    # return ids
                    else:
                        logger.error(
                            "get_ids for project does not require file or "
                            "folder name.")
                    sys.exit()

            if level == 'project':
                if fi is None and fdr is None:
                    # Must projects, check if we need to refer to folder_id
                    if father.style == "folder":
                        # aggregate all the ids from folders
                        ids.extend([self.folder_id[f][father.name] for f in
                                    self.folder_id])
                    elif father.style == "multifolder":
                        ids.extend(
                            [self.multifolder_id[f][father.name] for f in
                             self.multifolder_id])
                    else:
                        try:
                            ids.extend([self.folder_id[f][father.name] for f in
                                        self.folder_id])
                        except KeyError as _:
                            ids.extend(self.project_id[father.name])
                else:
                    logger.error(
                        "get_ids for project does not require file or "
                        "folder name.")
                    sys.exit()

            if level == "multifolder":
                if fi is None:
                    for fd in mfdr:
                        try:
                            ids.extend([self.folder_id[fd][father.name]])
                        except KeyError as _:
                            logger.error(
                                f"Could not find folder {fd} "
                                f"with father {father.name}")
                else:
                    logger.error(
                        "get_ids for project does not require file name")
                    sys.exit()
            if level == "folder":
                if fi is None:
                    if father.style == "file":
                        # aggregate all the ids from files
                        ids.extend([self.file_id[fdr][f][father.name] for f in
                                    self.file_id[fdr]])
                    else:
                        ids.extend([self.folder_id[fdr][father.name]])
                    # return ids
                else:
                    logger.error(
                        "get_ids for project does not require file name")
                    sys.exit()

            # this whole thing need serious refactoring
            if father is not None and father.name != "None" and \
                    fi is not None and fdr is not None:
                if father.style == "prepare":
                    ids.extend([self.prepare_id[father.name]])
                else:
                    ids.extend([self.file_id[fdr][fi][father.name]])

        return ids

    def get_all_ids(self):
        """Return a list with all the ids we have collected."""
        all_ids = []

        # prepare_id
        for _, ids in self.prepare_id.items():
            all_ids.append(ids)

        # file_id
        for _, folder in self.file_id.items():
            for _, file in folder.items():
                for _, ids in file.items():
                    all_ids.append(ids)

        # folder_id
        for _, folder in self.folder_id.items():
            for _, ids in folder.items():
                all_ids.append(ids)

        # project_id
        for _, lp in self.project_id.items():
            for ids in lp:
                all_ids.append(ids)

        return all_ids
