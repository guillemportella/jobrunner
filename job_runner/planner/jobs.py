""" Implement the definitions of the jobs and a function to launch them

Define helper classes to build and submit jobs.

The class inheritance is:

Submitter --> SetupJob --> Filter, Aligner, etc...

There are two main helper classes, Submitter and SetupJob.

Submitter provides a method to send jobs to the cluster and to create the
output folder. It adds a list of dependencies, based on queue job id, such
that a job won't start until all its dependencies have finished.

SetupJob inherits from Submitter, and stores the variables that are
needed to jobs. SetupJob is intended to be inherited by classes defining
actual jobs, and it enforces that these inherited classes implement a
build_cmd() method, which must return a string with the cmdline to be
executed in the cluster.

A job class must take as argument a JobConfig named tuple with all the
required options to run the job. Ultimately, when any job class has been
instantiated, one can execute the run() method to launch the job to
the queuing system. We only support SLURM by now, but we could add others
if we add, e.g., `fyrd` as a dependency.

For everything to work with the rest of the package, we must register
any job class in the registered_jobs dictionary, using the intended
job name as key and the class as value. N.B. the job name is the one used in
the input file, between square brackets.

Finally, dispatch_job() receives the job name/stage and a JobConfig variable,
launches the job (if we are not in testing mode), and returns the job id
of that job.
"""
import logging
import os
import sys
from abc import ABCMeta, abstractmethod
from typing import List, Dict, Union, Set
from typing import NamedTuple

from job_runner.planner.job_planner import Node
from job_runner.slurmpy import Slurm

logger = logging.getLogger("runner_logger")


class Submitter(object):
    """Base class to prepare a submission to SLURM"""

    def __init__(self, mem: str = "20G", *, cmd: str = None, name: str = None,
                 out_folder: str = None,
                 in_folder: str = None,
                 depends: List[int] = None):
        self.mem = mem
        self.cmd = cmd
        self.name = name
        self.depends = depends
        self.out_folder = out_folder
        self.in_folder = in_folder

    def make_out_folder(self):
        cwd = os.getcwd()
        full_path = os.path.join(cwd, self.out_folder)
        logger.debug(f"Making folder {full_path}")
        os.makedirs(full_path, exist_ok=True)

    def run(self) -> int:
        kwargs = {'mem': self.mem,
                  "kill-on-invalid-dep": "yes",
                  }
        s = Slurm(self.name, kwargs)
        return s.run(self.cmd, depends_on=self.depends)

    @staticmethod
    def log_cmd(*, cmd: str = None, job_id: int = None, file: str = None):
        with open(file, 'a') as fo:
            to_print = "\t".join([str(job_id), cmd])
            print(to_print, file=fo)


class JobConfig(NamedTuple):
    """Store the info required to run the jobs."""
    stage_name: str
    run_options: Dict
    folder: Union[str, List[str]]
    f_name: Union[str, List[str], NamedTuple]
    dep_ids: List[id]
    father: List[Node]
    test: bool
    job_counter: int
    log_file: str


class SetupJob(Submitter, metaclass=ABCMeta):
    """Parent class for all jobs.

    Inherits from Submitter class, which implements a method to submit
    to the SLURM queue in the cluster (run() method).
    It also inherits from ABCMeta metaclass to enforce that its sublcasses
    implement a build_cmd method, in which the job is specified.

    The SetupJobs class is initialized by passing a JobConfig namedtuple
    which carries all the info needed to execute the job.

    After we initialized the parent class we can create a folder
    where the output of each job should go.

    """

    def __init__(self, config: JobConfig = None):
        """Initializes the class by reading in Jobconfig.

        Jobconfig carries all the information that is typically required
        to set up a job: name of the files, folder, all the options of
        the project, the name of the current stage, etc...
        """
        self.stage_name = config.stage_name
        self.run_options = config.run_options
        self.folder = config.folder
        self.f_name = config.f_name
        self.files = config.f_name
        self.depends = config.dep_ids
        self.father = config.father
        self.test = config.test
        self.job_counter = config.job_counter
        self.log_file = config.log_file
        # self.memory = config.run_options['memory']
        self.memory = config.run_options['stages'][config.stage_name]['memory']
        logger.debug(f"Memory for this stage is {self.memory}")

        cmd = self.build_cmd()

        if len(self.f_name) == 0:
            sub_name = self.folder
        else:
            sub_name = self.f_name

        queue_name = f'{self.stage_name}_{sub_name}'

        super().__init__(cmd=cmd, name=queue_name,
                         out_folder=f"{self.stage_name}_{self.folder}",
                         depends=self.depends, mem=self.memory)
        self.make_out_folder()

    @abstractmethod
    def build_cmd(self) -> str:
        pass

    def kw_options(self) -> str:
        """Returns dict of key/value options.

        We don't return known options such as cmd, depends or dependency_type
        If we allow more than these options in the input as "required" we
        should add them here, or centralized them such that we don't add bugs

        """
        not_this = ['cmd', 'depends', 'dependency_type', 'reference',
                    'can_fail', 'memory', 'clean_up', 'short_options',
                    'key_value', 'enrich', 'control', 'paired_control',
                    'multifolder_postfix']
        opts = []
        run_options = self.run_options['stages'][self.stage_name]

        # some programs don't use options (ie. -- in front of options)
        # some (notably picard) don't use ticks but = .. fuckers)
        b_short = False
        b_keyvalue = False  # picard-like options, e.g. OPTION=VALUE

        if "short_options" in run_options:
            if run_options['short_options'] == "True":
                b_short = True

        if "key_value" in run_options:
            if run_options['key_value'] == "True":
                b_keyvalue = True

        for opt in run_options:
            if opt not in not_this:
                if run_options[opt] == "True":
                    if b_short:
                        opts.append("-" + opt)
                    else:
                        opts.append("--" + opt)
                elif b_short:
                    opts.append("-" + opt + " " + run_options[opt])
                elif b_keyvalue:
                    opts.append(opt + "=" + run_options[opt])
                else:
                    opts.append("--" + opt + " " + run_options[opt])
        return " ".join(opts)


###############################################################################
# Job definitions go here
#
# Implement your job classes here, and add them to the registered_job
# dictionary at the bottom of this file.
#
# Each job should inherit from SetupJob, and initialize the parent class
# with the Jobconfig named tuple. Each job must define a build_cmd()
# method that returns a string in which you describe the cmdline to
# execute.
#
# Checkout SetupJob to see the attributes you can access. The idea is
# that you read the inputs from the folder named after the father job
# and write the output to the folder named after the current job,
# or stage in our lingo.
#
###############################################################################

class Bcl2Fastq(SetupJob):
    """Prepare cmd to convert bcl files to fastq.

    TODO: check if the cvs input file for bcl2fast is in the path
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        cmd = stage_options['cmd']
        keyword_options = self.kw_options()
        cmd = f"{cmd} " \
            f"{keyword_options}"
        return cmd


class TrimmerCutAdapt(SetupJob):
    """Prepares a command to launch cutadapt.

    Accepts key-word arguments to be passed to cutadapt. The important ones
    are the sequence of the adapters, these are required if you want the
    job to succeed. Writes the stdout of cutadapt to a fname_report.txt
    file in the output directory.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        cmd = stage_options['cmd']
        extension = self.run_options['regex']['extension']
        out_folder = f"{self.stage_name}_{self.folder}"
        keyword_options = self.kw_options()

        if not self.run_options['b_paired']:
            read_1 = self.run_options['regex']['read_one']
            out_rep = f"{self.f_name}{read_1}{extension}_trimming_report.txt"
            # the extension is fq.gz to make it the same as trim_galore which
            # i can't control, and it is all then compatible with the aligner
            out_trim = f"{self.f_name}{read_1}_trimmed.fq.gz"

            cmd = f"{cmd} " \
                f"{keyword_options} " \
                f"-o {out_folder}/{out_trim} " \
                f"{self.folder}/{self.f_name}{read_1}{extension} > " \
                f"{out_folder}/{out_rep}"
            pass
        else:
            read_1 = self.run_options['regex']['read_one']
            read_2 = self.run_options['regex']['read_two']
            out_rep = f"{self.f_name}{read_1}{extension}_trimming_report.txt"
            # the extension is fq.gz to make it the same as trim_galore which
            # i can't control, and it is all then compatible with the aligner
            out_trim1 = f"{self.f_name}{read_1}_trimmed.fq.gz"
            out_trim2 = f"{self.f_name}{read_2}_trimmed.fq.gz"
            cmd = f"{cmd} " \
                f"{keyword_options} " \
                f"-o {out_folder}/{out_trim1}  " \
                f"-p {out_folder}/{out_trim2}  " \
                f"{self.folder}/{self.f_name}{read_1}{extension} " \
                f"{self.folder}/{self.f_name}{read_2}{extension} > " \
                f"{out_folder}/{out_rep}"
        return cmd


class TrimmerGalore(SetupJob):
    """Prepares a command to launch Trim_Galore.

    Accepts key-word arguments to Trim_Galore, e.g. quality thresholds.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        cmd = stage_options['cmd']
        extension = self.run_options['regex']['extension']
        out_folder = f"{self.stage_name}_{self.folder}"
        keyword_options = self.kw_options()

        if not self.run_options['b_paired']:
            read_1 = self.run_options['regex']['read_one']
            cmd = f"{cmd} " \
                f"-o {out_folder} " \
                f"{self.folder}/{self.f_name}{read_1}{extension} " \
                f"{keyword_options} "
            pass
        else:
            read_1 = self.run_options['regex']['read_one']
            read_2 = self.run_options['regex']['read_two']
            cmd = f"{cmd} " \
                f"-o {out_folder} " \
                f"--paired {self.folder}/{self.f_name}{read_1}{extension} " \
                f"{self.folder}/{self.f_name}{read_2}{extension} " \
                f"{keyword_options} "
        return cmd


class AlignerBWA(SetupJob):
    """Align reads using BWA makes a bam file out of them"""

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def validate_input(self):
        stage_options = self.run_options['stages'][self.stage_name]
        if stage_options['dependency_type'] != 'file':
            logger.error('Can only run align in if dependency_type = file ')
            sys.exit()

    def build_cmd(self) -> str:
        self.validate_input()
        stage_options = self.run_options['stages'][self.stage_name]
        reference = stage_options['reference']
        cmd = stage_options['cmd']
        keyword_options = self.kw_options()
        father = next(iter(self.father)).name
        inp_folder = f"{father}_{self.folder}"
        out_folder = f"{self.stage_name}_{self.folder}"

        if not self.run_options['b_paired']:
            read_1 = self.run_options['regex']['read_one']
            cmd = f"{cmd} {keyword_options} {reference} " \
                f"{inp_folder}/{self.f_name}{read_1}_trimmed.fq.gz " \
                f"| samtools view -bS -@8 - " \
                f"-o {out_folder}/{self.f_name}.bam "
        else:
            read_1 = self.run_options['regex']['read_one']
            read_2 = self.run_options['regex']['read_two']
            cmd = f"{cmd} {keyword_options} {reference} " \
                f"{inp_folder}/{self.f_name}{read_1}_val_1.fq.gz " \
                f"{inp_folder}/{self.f_name}{read_2}_val_2.fq.gz " \
                f"| samtools view -bS -@8 - " \
                f"-o {out_folder}/{self.f_name}.bam "
        return cmd


class AlignerBowtie2(SetupJob):
    """Align using Bowtie2 reads and makes a bam file out of them.

    It also indexes the final bam
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def validate_input(self):
        stage_options = self.run_options['stages'][self.stage_name]
        if stage_options['dependency_type'] != 'file':
            logger.error('Can only run align in if dependency_type = file ')
            sys.exit()

    def build_cmd(self) -> str:
        self.validate_input()
        stage_options = self.run_options['stages'][self.stage_name]
        reference = stage_options['reference']
        cmd = stage_options['cmd']
        keyword_options = self.kw_options()
        father = next(iter(self.father)).name
        inp_folder = f"{father}_{self.folder}"
        out_folder = f"{self.stage_name}_{self.folder}"

        if not self.run_options['b_paired']:
            read_1 = self.run_options['regex']['read_one']
            cmd = f"{cmd} {keyword_options} -x {reference} " \
                f"-U {inp_folder}/{self.f_name}{read_1}_trimmed.fq.gz " \
                f"| samtools sort -@8 - " \
                f"-o {out_folder}/{self.f_name}.bam && " \
                f"" \
                f"samtools index {out_folder}/{self.f_name}.bam"
        else:
            read_1 = self.run_options['regex']['read_one']
            read_2 = self.run_options['regex']['read_two']
            cmd = f"{cmd} {keyword_options} -x {reference} " \
                f"-1 {inp_folder}/{self.f_name}{read_1}_val_1.fq.gz " \
                f"-2 {inp_folder}/{self.f_name}{read_2}_val_2.fq.gz " \
                f"| samtools sort -@8 - " \
                f"-o {out_folder}/{self.f_name}.bam && " \
                f"" \
                f"samtools index {out_folder}/{self.f_name}.bam"
        return cmd


class AlignSort(SetupJob):
    """Align reads using BWA mem and make a sorted bam file out of them.

    Accepts key words for the aligner only.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        reference = stage_options['reference']
        cmd = stage_options['cmd']
        keyword_options = self.kw_options()
        father = next(iter(self.father)).name
        inp_folder = f"{father}_{self.folder}"
        out_folder = f"{self.stage_name}_{self.folder}"

        if not self.run_options['b_paired']:
            read_1 = self.run_options['regex']['read_one']
            cmd = f"{cmd} {keyword_options} {reference} " \
                f"{inp_folder}/{self.f_name}{read_1}_trimmed.fq.gz " \
                f"| samtools sort -@8 - " \
                f"-o {out_folder}/{self.f_name}.bam && " \
                f"" \
                f"samtools index {out_folder}/{self.f_name}.bam"
        else:
            read_1 = self.run_options['regex']['read_one']
            read_2 = self.run_options['regex']['read_two']
            cmd = f"{cmd} {keyword_options} {reference} " \
                f"{inp_folder}/{self.f_name}{read_1}_val_1.fq.gz " \
                f"{inp_folder}/{self.f_name}{read_2}_val_2.fq.gz " \
                f"| samtools sort -@8 - " \
                f"-o {out_folder}/{self.f_name}.bam && " \
                f"" \
                f"samtools index {out_folder}/{self.f_name}.bam"
        return cmd


class SorterBam(SetupJob):
    """Sort a bam file.

    Accepts key words for the sorting command.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        cmd = stage_options['cmd']
        keyword_options = self.kw_options()
        father = next(iter(self.father)).name
        inp_folder = f"{father}_{self.folder}"
        out_folder = f"{self.stage_name}_{self.folder}"

        if stage_options['dependency_type'] != "file":
            f_name = self.folder
        else:
            f_name = self.f_name

        cmd = f"{cmd} -T /tmp/{inp_folder} {keyword_options} " \
            f" {inp_folder}/{f_name}.bam >" \
            f" {out_folder}/{f_name}.bam"
        return cmd


class MergerBam(SetupJob):
    """Merge, count reads from bams.

    Accepts key words for the merge command.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def validate_input(self):
        stage_options = self.run_options['stages'][self.stage_name]
        if stage_options['dependency_type'] != 'folder':
            logger.error('Can only run merge in if dependency_type = folder ')
            sys.exit()

    def build_cmd(self) -> str:
        self.validate_input()
        stage_options = self.run_options['stages'][self.stage_name]
        keyword_options = self.kw_options()
        cmd = stage_options['cmd']
        father = next(iter(self.father)).name
        stage = self.stage_name
        folder = self.folder
        names = [f"{father}_" + folder + "/" + x + ".bam" for x in
                 self.files]
        all_files = " ".join(names)
        names = [f"{father}_" + folder + "/" + x + ".bam.bai" for x in
                 self.files]
        all_index = " ".join(names)

        cmd = f"{cmd} {keyword_options} " \
            f"{stage}_{folder}/{folder}.bam" \
            f" {all_files} && " \
            f"" \
            f"echo 'Done merging, clean up' && " \
            f"" \
            f"rm -fr {all_files} && " \
            f"rm -fr {all_index} && " \
            f"" \
            f"samtools index {stage}_{folder}/{folder}.bam && " \
            f"" \
            f"tot_reads=$(samtools flagstat " \
            f"{self.stage_name}_{self.folder}/{self.folder}.bam " \
            f"| head -n 1 | awk '{{print $1}}')"

        return cmd


class FilterDuplicates(SetupJob):
    """Markduplicates, filter and index in one go.

    This is a very common workflow used when filtering.
    The sam include and sam exclude values are preset to 3 and 3840,
    if not given in the input file.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        picard = stage_options['cmd']
        whitelist = stage_options['whitelist']
        # defaults, this is how I usually filter (GP)
        sam_include = 3
        sam_exclude = 3840
        sam_include = int(stage_options.get('sam_include', sam_include))
        sam_exclude = int(stage_options.get('sam_exclude', sam_exclude))
        father = next(iter(self.father)).name
        stage = self.stage_name
        folder = self.folder

        # f"rm -fr {father}_{folder}/{folder}.bam &&"
        # f""  \
        cmd = f"java -Xmx3G -jar {picard} MarkDuplicates " \
            f"VALIDATION_STRINGENCY=SILENT " \
            f"I={father}_{folder}/{folder}.bam " \
            f"O={stage}_{folder}/{folder}.markdup.bam " \
            f"M={stage}_{folder}/{folder}.markdup.txt && " \
            f"" \
            f"echo 'Duplicates marked' && " \
            f"" \
            f"samtools view {stage}_{folder}/{folder}.markdup.bam " \
            f"-f {sam_include} -F {sam_exclude} -q 10 -b -L {whitelist} " \
            f"| samtools sort -@ 20 -T /tmp/{folder} " \
            f" -o {stage}_{folder}/{folder}.bam - && " \
            f"" \
            f"echo 'Done sorting, indexing bam.' && " \
            f"" \
            f"samtools index {stage}_{folder}/{folder}.bam && " \
            f"" \
            f"echo 'Done indexing, remove intermediate file.' && " \
            f"" \
            f"rm -fr {stage}_{folder}/{folder}.markdup.bam && " \
            f"" \
            f"echo 'Count reads after filtering.'  && " \
            f"" \
            f"filt_reads=$(samtools flagstat " \
            f"{stage}_{folder}/{folder}.bam " \
            f"| head -n 1 | awk '{{print $1}}' )"

        # FIXME --> scratch space for writing sorting intermediates, tmp ok?
        return cmd


class MarkDuplicates(SetupJob):
    """Mark duplicates.

    We use Picard to mark the duplicates.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        cmd = stage_options['cmd']
        father = next(iter(self.father)).name
        stage = self.stage_name
        folder = self.folder

        if stage_options['dependency_type'] != "file":
            f_name = folder
        else:
            f_name = self.f_name

        cmd = f"java -Xmx3G -jar {cmd} MarkDuplicates " \
            f"VALIDATION_STRINGENCY=SILENT " \
            f"I={father}_{folder}/{f_name}.bam " \
            f"O={stage}_{folder}/{f_name}.bam " \
            f"M={stage}_{folder}/{f_name}.txt  " \
            f""
        return cmd


class FilterBam(SetupJob):
    """Filtering bams.

    Pass your filtering options by means of key word arguments, e.g F and f.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        cmd = stage_options['cmd']
        keyword_options = self.kw_options()
        father = next(iter(self.father)).name

        if stage_options['dependency_type'] != "file":
            f_name = self.folder
        else:
            f_name = self.f_name

        inp_folder = f"{father}_{self.folder}"
        out_folder = f"{self.stage_name}_{self.folder}"

        cmd = f"{cmd} {keyword_options} " \
            f" {inp_folder}/{f_name}.bam >" \
            f" {out_folder}/{f_name}.bam && " \
            f"samtools index {out_folder}/{f_name}.bam "

        return cmd


class Quality(SetupJob):
    """Launch quality control for a specific folder.

    The quality control tool is hardcoded to fastqc, and we don't accept
    any key words for it.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        folder = self.folder
        father = next(iter(self.father)).name
        stage = self.stage_name

        cmd = f"fastqc {folder}/*.fq.gz {folder}/*.fastq.gz " \
            f" -o {stage}_{folder} && " \
            f"" \
            f"fastqc {father}_{folder}/*.fq.gz " \
            f" {father}_{folder}/*.fastq.gz -o {stage}_{folder} " \
            f""

        return cmd

class Mosdepth(SetupJob):
    """Calculate coverage

    Generate a default report from mosdepth 
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options["stages"][self.stage_name]
        cmd = stage_options["cmd"]
        keyword_options = self.kw_options()
        father = next(iter(self.father)).name

        if stage_options["dependency_type"] != "file":
            f_name = self.folder
        else:
            f_name = self.f_name

        inp_folder = f"{father}_{self.folder}"
        out_folder = f"{self.stage_name}_{self.folder}"

        cmd = (
            f"{cmd} {keyword_options} "
            f" {out_folder}/{f_name}"
            f" {inp_folder}/{f_name}.bam "
            f""
        )

        return cmd

class SamStats(SetupJob):
    """Calculate number of aligned reads, etc. using samtools stats.

    Generate a default report from samtools stats
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        cmd = stage_options['cmd']
        keyword_options = self.kw_options()
        father = next(iter(self.father)).name

        if stage_options['dependency_type'] != "file":
            f_name = self.folder
        else:
            f_name = self.f_name

        inp_folder = f"{father}_{self.folder}"
        out_folder = f"{self.stage_name}_{self.folder}"

        cmd = f"{cmd} {keyword_options} " \
            f" {inp_folder}/{f_name}.bam " \
            f" > {out_folder}/{f_name}.stats" \
            f""

        return cmd


class ChIPSeq(SetupJob):
    """Perform peak calling using MACS2, typically for ChIP-seq
    
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        cmd = stage_options['cmd']
        keyword_options = self.kw_options()
        father = next(iter(self.father)).name
        treatment = self.f_name.enrich
        control = self.f_name.control
        treatment_bam = father + "_" + treatment + "/" \
                        + treatment + ".bam"
        control_bam = father + "_" + control + "/" \
                      + control + ".bam"

        out_name = "chipseq_" + self.folder
        out_dir = out_name

        if self.run_options['b_paired']:
            type_reads = "BAMPE"
        else:
            type_reads = "BAM"

        cmd = (
            f"{cmd} {keyword_options}--keep-dup all --bdg "
            f"-f {type_reads} -t {treatment_bam} -c {control_bam} "
            f"-g hs -n {out_name} --outdir {out_dir}"
        )
        return cmd


class Preseq(SetupJob):
    """Predict and estimate the complexity of a genomic sequencing library

    Runs preseq on bam files. For the results to make sense, the files
    must include duplicates.
    Do not pass the -P / -pe option yourself, it will be determined based
    on the type of sequencing you've specified in [input_type] section of the
    input file.
    Do not pass the -B option yourself, we only expect bam files so it
    is hardcoded.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        stage_options = self.run_options['stages'][self.stage_name]
        cmd = stage_options['cmd']
        keyword_options = self.kw_options()
        father = next(iter(self.father)).name

        if stage_options['dependency_type'] != "file":
            f_name = self.folder
        else:
            f_name = self.f_name

        inp_folder = f"{father}_{self.folder}"
        out_folder = f"{self.stage_name}_{self.folder}"

        p_end = " "
        if self.run_options['b_paired']:
            p_end = "-P"

        # I don't add a && after the first execution because it might
        # fail but I still want the next line to be executed
        cmd = f"{cmd} c_curve {keyword_options} -B {p_end} " \
            f" -o {out_folder}/{f_name}_c_curve.txt" \
            f" {inp_folder}/{f_name}.bam " \
            f" \n" \
            f"{cmd} lc_extrap {keyword_options} -B {p_end} " \
            f" -o {out_folder}/{f_name}_yield.txt" \
            f" {inp_folder}/{f_name}.bam " \
            f""

        return cmd


class MultiQC(SetupJob):
    """Launch multiqc for a given run.

    By run we mean all the folders in you input file...
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        project_options = self.run_options['project']
        report_name = project_options['report_name']
        safer_report_name = report_name.replace(" ", "_")
        stage = self.stage_name
        folder = self.folder

        cmd = f"multiqc . -f -o {stage}_{folder} -i {report_name} " \
            f"-n {safer_report_name}.html " \
            f"--no-data-dir"
        return cmd


class SubmitReport(SetupJob):
    """Send the multiqc report.

    You would need a .env file present in the directory, with the USER_RUNNER
    and USER_PWD variables defined. Not very secure right now.
    """

    def __init__(self, config: JobConfig = None):
        super().__init__(config)

    def build_cmd(self) -> str:
        project_options = self.run_options['project']
        folder = self.folder
        father = next(iter(self.father)).name
        prj = project_options['project_name']
        run = project_options['run_name']
        report = project_options['report_name']
        safer_report = report.replace(" ", "_")
        # TODO --> need to have a way to check for .env being present
        cmd = "source  .env && " \
            f"" \
              'submit_report.py -u "${USER_RUNNER}" -p "${USER_PWD}" ' \
            f"-f {father}_{folder}/{safer_report}.html " \
            f"project -p {prj} -r {run} -fname {safer_report} "

        return cmd


# This one is special, used to clean up. I choose to do it as a job submitted
# to the cluster because the user might decide not to use the daemon.


class SubmitCleanUp(Submitter):
    """Send a job removing all the folders that have been deemed removable

    The user can add an option ('clean_up = True) in the input file to mark
    that the folder from a particular job can be removed at the end.
    """

    def __init__(self, folders_to_clean: Set = None, depends: List = None):

        if folders_to_clean is not None:
            cmd = self.build_cmd(folders_to_clean)
        else:
            # at least we are not doing any harm
            cmd = ""
        super().__init__(cmd=cmd, name="cleaner",
                         out_folder=f"cleaner_folder",
                         depends=depends, mem="60G")

    @staticmethod
    def build_cmd(folders: Set) -> str:
        """Build the cmd line to remove all the folders from the list"""
        f = " ".join(folders)
        cmd = f"rm -fr  {f}"
        return cmd


###############################################################################
# Job definitions end here
###############################################################################


# Add any new class in this dictionary. Each key is the name
# of the job in the input file, and the value is the class that
# will eventually execute the job.

registered_jobs = {
    'bcl2fastq': Bcl2Fastq,
    'trimming_cutadapt': TrimmerCutAdapt,
    'trimming_galore': TrimmerGalore,
    'align_bwa': AlignerBWA,
    'align_bt2': AlignerBowtie2,
    'align_bwa_sort': AlignSort,
    'sortbam': SorterBam,
    'mergebam': MergerBam,
    'markduplicates': MarkDuplicates,
    'filterbam': FilterBam,
    'filterduplicates': FilterDuplicates,
    'quality': Quality,
    'chipseq': ChIPSeq,
    'samstats': SamStats,
    'preseq': Preseq,
    'multiqc': MultiQC,
    'mosdepth': Mosdepth,
    'submit': SubmitReport,
}


def dispatch_job(stage: str = None, config: JobConfig = None) -> int:
    """Configure and run a given job."""
    job_class = registered_jobs[stage]
    job_o = job_class(config)
    if job_o.test:
        return job_o.job_counter
    else:
        job_id = job_o.run()
    job_o.log_cmd(cmd=job_o.cmd, job_id=job_id, file=job_o.log_file)
    return job_id
