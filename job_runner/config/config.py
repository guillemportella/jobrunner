import getpass
import os

from dotenv import load_dotenv

load_dotenv()

log_fname = f"/tmp/runner_logger_{getpass.getuser()}.log"


class BasicConfig(object):
    DEBUG = os.getenv("DEBUG")
    INPUT_FILE = ""
    NO_DAEMON = ""

    if "TEST" not in os.environ:
        TEST = False
    else:
        TEST = os.getenv('TEST')
    if "LOG_FILE" not in os.environ:
        LOG_FILE = log_fname
    else:
        LOG_FILE = os.getenv("LOG_FILE")
