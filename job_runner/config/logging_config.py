""" Configure the logging module.

Details a bunch of configuration options in a dictionary, and uses it
to initialize the logging object.

"""
import getpass
import logging.config
import os
import sys
from collections import namedtuple
from pathlib import Path  # python3 only

from dotenv import load_dotenv

dotenv_path = Path('.') / '.env'
load_dotenv(dotenv_path=dotenv_path)

##########################################################################
# Logging configuration
# It should read the 'DEBUG' config from above
##########################################################################

# I just output 'info' level by default, unless debugging
output_level = 'INFO'
if 'DEBUG' in os.environ:
    debug_level = os.getenv('DEBUG')
    if debug_level == "True":
        output_level = 'DEBUG'

log_fname = f"/tmp/runner_logger_{getpass.getuser()}.log"
if "LOG_FILE" not in os.environ:
    LOG_FILE = log_fname
else:
    LOG_FILE = os.getenv("LOG_FILE")

# halt if logger file is not writable!!

try:
    with open(LOG_FILE, "w") as fo:
        pass
except OSError as e:
    print(f"ERROR: Your log file\n"
          f"{LOG_FILE}\n"
          f"is not writable.\n\n"
          f"SOLUTION: define a .env file in the present folder and define "
          f"the location of a log file that you can write into. E.g.\n\n"
          f"LOG_FILE=place_where_you_can_write.log\n")
    sys.exit()


# Custom formatter
class MyFormatter(logging.Formatter):
    err_fmt = "ERROR: %(msg)s"
    wrng_fmt = "Warning: %(msg)s"
    dbg_fmt = "DBG: %(module)s: %(lineno)d: %(message)s"
    info_fmt = "%(msg)s"

    def __init__(self):
        super().__init__(fmt="%(levelno)d: %(message)s", datefmt=None,
                         style='%')

    def format(self, record):

        # Save the original format configured by the user
        # when the logger formatter was instantiated
        format_orig = self._style._fmt

        # Replace the original format with one customized by logging level
        if record.levelno == logging.DEBUG:
            self._style._fmt = MyFormatter.dbg_fmt

        elif record.levelno == logging.WARNING:
            self._style._fmt = MyFormatter.wrng_fmt

        elif record.levelno == logging.INFO:
            self._style._fmt = MyFormatter.info_fmt

        elif record.levelno == logging.ERROR:
            self._style._fmt = MyFormatter.err_fmt

        # Call the original formatter class to do the grunt work
        result = logging.Formatter.format(self, record)

        # Restore the original format configured by the user
        self._style._fmt = format_orig

        return result


FORMATTER_FACTORY = '()'

LOG_SETTINGS = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': output_level,
            'formatter': 'custom',
            'stream': 'ext://sys.stdout',
        },
        'file_runner': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': output_level,
            'formatter': 'semidetailed',
            'filename': LOG_FILE,
            'mode': 'a',
            'maxBytes': 10485760,
            'backupCount': 5,
        },

    },
    'formatters': {
        'detailed': {
            'format': '%(asctime)s %(module)-17s line:%(lineno)-4d ' \
                      '%(levelname)-8s %(message)s',
        },
        'email': {
            'format': 'Timestamp: %(asctime)s\nModule: %(module)s\n' \
                      'Line: %(lineno)d\nMessage: %(message)s',
        },
        'simple': {
            'format': '[%(levelname)-7s] %(module)8s : %(message)s',
        },
        'custom': {
            FORMATTER_FACTORY: MyFormatter,
        },
        'plain_print': {
            'format': '%(message)s',
        },
        'semidetailed': {
            'format': '[%(asctime)s] -' \
                      ' [%(levelname)-7s] %(module)15s : %(message)s',
        }
    },
    'loggers': {
        'runner_logger': {
            'level': output_level,
            'handlers': ['file_runner', 'console'],
            'propagate': False,
        },
        'root': {
            'level': output_level,
            'handlers': ['console']
        },

    },
}

Config = namedtuple('configuration', ['LOG_SETTINGS'])
config_logging = Config(LOG_SETTINGS)
logging.config.dictConfig(config_logging.LOG_SETTINGS, )
