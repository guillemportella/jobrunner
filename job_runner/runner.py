"""Implements the runner class, used to submit the jobs to the cluster.

This class can only be instantiated once. We use a factory function to
prepare the runner class, reading the input files, parsing and checking them.
This allows to inject different options when doing testing.

When the run() method in the runner class is executed, the first thing it
checks is if a pidlock file exists, meaning that has already started the same
job. If it's the case, it exits.
Then it creates an execution plan based on the implementation of a DAG defined
in `planner/job_planner.py`. After that it will try to find the required files
in the required folders, and proceed to launch the jobs. It keeps track
of the job ids and their dependencies, information that is also useful later on
for monitoring the jobs.

Unless the user decides otherwise, we start switch to daemon mode and start
the cluster monitoring tool whence all the jobs have been submitted.


"""
import fnmatch
import json
import logging
import os
import pathlib
import re
import shutil
import sys
from collections import namedtuple
from typing import Dict, Tuple, Generator

import parse
from colorama import Fore
from colorama import init

logger = logging.getLogger("runner_logger")

# colorama initializations
init()

from pid import PidFile, PidFileAlreadyRunningError, PidFileAlreadyLockedError

from job_runner.config import config_logging
from job_runner.daemonize.runner_daemon import enter_daemon, PID_FOLDER
from job_runner.parse.example_input import example_input
from job_runner.planner.job_planner import DependencyTree, StoreIds
from job_runner.planner.jobs import registered_jobs, JobConfig, dispatch_job, \
    SubmitCleanUp
from .config.config import BasicConfig
from .parse.parser import parse_input_file

logging.config.dictConfig(config_logging.LOG_SETTINGS)
logger = logging.getLogger("runner_logger")

DESC_LAUNCH = """
Cluster job runner.

Specify the jobs that need to be carried out in an input file.

Hint: add the -example flag and get an example input file.

"""

# create namedtuple and add it to the list chipseq files
pairs_chipseq = namedtuple('ChIPSeqPairs', ['enrich', 'control'])


def clean_string(s: str) -> str:
    ss = s.replace(" ", "_"). \
        replace("/", "_"). \
        replace('"', "_"). \
        replace("'", "_"). \
        replace("*", "_"). \
        replace("?", "_")
    return ss


def run_job(stage, job_config):
    try:
        job = registered_jobs[stage]
    except KeyError as e:
        logger.error("I do not know about job type 'stage'.")
        sys.exit()

    return job(job_config).run()


class ChiPSeqPrep(object):
    """ Find the right pairs of files to run ChipSeq on.

    ChIP-seq input preparation is a bit special, compared to the rest of the
    jobs as there are constraints in the pairs of files, and the number
    of jobs executed depend on their number. Hence, this class is implemented
    here and not in jobs.py """

    def __init__(self, config, chipseq_opt, father):
        self.config_obj = config
        self.chipseq_opt = chipseq_opt
        self.father = father

    @staticmethod
    def _find_enrich(father, regex) -> Generator[str, None, None]:
        """Generate the path of each enrichment file"""
        to_match = father + "*" + regex
        for f in pathlib.Path(".").iterdir():
            if f.is_dir() & fnmatch.fnmatch(f, to_match):
                yield f.name.replace(father + "_", "")

    def _find_control(self, common_root) -> Generator[str, None, None]:
        """Generate the names for the control/input"""
        father = next(iter(self.father)).name

        if self.chipseq_opt['paired_control'] == 'yes' \
                or self.chipseq_opt['paired_control'] == 'true':
            logger.error("Paired ChIP-seq not yet implemented")
            sys.exit()
        else:
            putative_ctrl = father + "_" + common_root + \
                            self.chipseq_opt['control']
            if pathlib.Path(putative_ctrl).exists():
                yield common_root + self.chipseq_opt['control']
            else:
                logger.debug(f"Can not find {putative_ctrl}")
                return

    @staticmethod
    def _find_common_root(file, regex) -> Tuple[str, str]:
        """Get the common root of all files.

        Parsing implies that the names of the enrichment files end with
        a digit indicating their technical repetition id, and that we
         use a glob expression with a * indicating that. """
        to_parse = "{common}" + regex.replace("*", "{N}")
        found = parse.parse(to_parse, file)
        if found is not None:
            return found["common"], found["N"]
        else:
            sys.exit("Could not find a common name. Make sure it's of"
                     "the form _NAME*.")

    def find_pairs(self) -> Dict:
        father = next(iter(self.father)).name
        track_replicates = {}
        pairs = {}
        for count_enrich, enrich in enumerate(
                self._find_enrich(father, self.chipseq_opt['enrich'])):
            common_root, count = \
                self._find_common_root(enrich, self.chipseq_opt['enrich'])
            track_replicates.setdefault(common_root, 0)
            track_replicates[common_root] += 1
            p_enrich = pathlib.Path(father + "_" + enrich)
            for count_ctrl, ctrl in enumerate(
                    self._find_control(common_root)):
                p_ctrl = pathlib.Path(father + "_" + ctrl)
                if pathlib.Path(p_ctrl).exists() & pathlib.Path(
                        p_enrich).exists():
                    how_many = track_replicates[common_root]
                    pairs.setdefault(common_root + "_tech" + str(how_many),
                                     pairs_chipseq(enrich=enrich,
                                                   control=ctrl))
                else:
                    sys.exit(
                        f"ERROR: can not file input for {ctrl} or {enrich}"
                    )
        return pairs


class Singleton(type):
    """Singleton metaclass.

    To guarantee that a class can only be instantiated once. Used for
    the Runner class, as it only makes sense to have one object.
    If you try to instantiate more than one, you will get back the same
    object, but it will run __init__ again (maybe you've changed the settings?)
    """

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                *args, **kwargs
            )
        else:
            cls._instances[cls].__init__(*args, **kwargs)

        return cls._instances[cls]


class Runner(metaclass=Singleton):
    """Main class to execute the jobs in the cluster."""

    def __init__(self, run_options, config_obj):
        self.config_obj = config_obj
        self.run_options = run_options
        self.unique_basenames = {}
        self.unique_multifolder = {}
        self.found_files = {}
        self.test_counter = 0  # only for testing
        # add if in case single end?

    def __repr__(self):
        logger.info("Job runner object.")

    def _prepare_files(self):
        self._find_unique_files()

    def _find_files(self):
        """Discover all the pairs of Pair end files in the folder"""
        pwd = self.run_options["input_folders"]
        cwd = os.getcwd()
        reg = self.run_options["regex"]["regex"]
        files = {}
        for name in pwd:
            directory = cwd + "/" + name
            regex = name + reg
            matched = []
            try:
                for file in os.listdir(directory):
                    if fnmatch.fnmatch(file, regex):
                        matched.append(file)
            except FileNotFoundError as _:
                logger.error(f"Could not find folder {directory}")
                sys.exit()
            if self.run_options["b_paired"]:
                if len(matched) % 2 == 0:
                    files[name] = matched
                else:
                    logger.error(f"I found an odd number of files for {name}")
                    sys.exit()
            else:
                files[name] = matched

        self.found_files = files

    def _find_unique_files(self):

        reg_no_lane_ini = self.run_options["regex"]["no_lane"]
        self._find_files()
        reg_no_lane = fnmatch.translate(reg_no_lane_ini)
        unique_files = {}
        for folder, non_unique in self.found_files.items():
            unique = set()
            for f in non_unique:
                basename = re.sub(reg_no_lane, "", f)
                unique.add(basename)
            unique_files[folder] = unique

        self.unique_basenames = unique_files

    def _find_multifolders_deps(self, stage: str = None, father=None):
        """Compose the names of folders a multifolder dependds

        Group by common stem, files which have the postfixs in
        common
        """

        if stage == "chipseq":
            opt = self.run_options['stages'][stage]
            chip = ChiPSeqPrep(self.config_obj, opt, father)
            common_folder = chip.find_pairs()
            pass
        else:
            postfix = self.run_options['stages'][stage][
                'multifolder_postfix'].split()

            # unique "stems" for files that share the same postifx
            common_folder = {}
            for pf in postfix:
                regex = "*" + pf
                for f in self.unique_basenames:
                    if fnmatch.fnmatch(f, regex):
                        rx = fnmatch.translate(pf)
                        bname = re.sub(rx, "", f)
                        common_folder.setdefault(bname, []).append(f)

        self.unique_multifolder = common_folder

    @staticmethod
    def create_cmd_log(*, folder: str = None) -> str:
        cwd = os.getcwd()
        target_dir = os.path.join(cwd, folder)
        log_file = target_dir + "/cmd_log.txt"
        open(log_file, "w").close()
        return log_file

    def _halt_if_pidfile(self) -> Tuple[str, str]:
        """Stop if the is a pidfile already

        Prevent the jobs from launching if we already have a monitoring
        system (we know because we should have pid file. If the user
        removed that, he deserves what's coming.
        """

        home_pids = pathlib.Path.home() / PID_FOLDER
        pathlib.Path(home_pids).mkdir(exist_ok=True)
        p_n = clean_string(self.run_options["project"]['project_name'])
        r_n = clean_string(self.run_options["project"]['run_name'])
        re_n = clean_string(self.run_options["project"]['report_name'])
        pid_file_name = "_".join([p_n, r_n, re_n])
        pid_n = os.getpid()
        server_name = "_".join([p_n, r_n, str(pid_n)]).lower()
        logger.debug(f"Home PID folder is {home_pids}")
        logger.debug(f"PID is {pid_file_name}")

        try:
            with PidFile(pid_file_name, piddir=home_pids) as _:
                pass
        except (PidFileAlreadyRunningError, PidFileAlreadyLockedError,
                BlockingIOError) as _:
            logger.info(
                f"\n{Fore.YELLOW}It appears you have an instance of jobrunner "
                f"active for this particular job.{Fore.RESET}\n"
                f"{Fore.RED}>>>>>>>>>>>>>>>>>>>>> I will not launch the jobs "
                f"again <<<<<<<<<<<<<<<<<<<<<{Fore.RESET}\n"
                f"\n"
                "If this is not true, and you are sure about it, try to "
                f"delete the file:\n{home_pids}/{pid_file_name}.pid")
            sys.exit()
        return server_name, pid_file_name

    def is_clean_up(self, stage: str = None) -> bool:
        """Decide if we will remove the folder after everything is done.

        Reads the configuration options of a given stage and returns
        true if the user set clean_up to True or true.

        :rtype: bool: True if the clean_up option is set to True/true
        :param stage: str: name of the job

        """
        try:
            value_clean = self.run_options['stages'][stage]['clean_up']
        except KeyError as e:
            # we don't care
            return False

        if value_clean == "True" or value_clean == 'true':
            return True

        return False

    def run(self):
        """Prepare and send the executions to the cluster.

        This is the only method that should be directly used. It starts
        by making sure we are not running the same project already, and
        it traces the order of the jobs based on the specified dependencies.
        If then goes stage by stage submitting the jobs, making sure that the
        dependencies are accounted for.
        Unless requested by the user, we will finally launch a daemon that
        will monitor the progress of the jobs.
        """

        server_name, pid_file_name = self._halt_if_pidfile()
        to_remove = []  # folder names to be removed after all jobs are done

        dependencies = DependencyTree()
        id_store = StoreIds()
        exec_plan = dependencies.get_plan(
            stages_options=self.run_options["stages"]
        )
        logger.debug(f"DEPENDENCY GRAPH")
        logger.debug(exec_plan)

        job_counter = 0
        log_file = self.create_cmd_log(folder="")
        for stage in exec_plan['prepare']:
            logger.debug(f"==> At stage: [{stage}]")
            father = dependencies.get_father(stage)
            dep_ids = id_store.get_depending_ids(fathers=father,
                                                 level='prepare')

            conf_job = JobConfig(
                stage_name=stage,
                run_options=self.run_options,
                folder="prepare",
                f_name="",
                dep_ids=dep_ids,
                father=father,
                test=self.config_obj.TEST,
                job_counter=job_counter,
                log_file=log_file,
            )

            job_id = dispatch_job(stage, conf_job)

            id_store.add_id(st=stage, job_id=job_id, level='prepare')
            id_store.add_fail(job_id, stage, self.run_options)

            if self.is_clean_up(stage):
                to_remove.append(f"{conf_job.stage_name}"
                                 f"_{conf_job.folder}")

            if father is not None:
                dep_names = [x.name for x in father if x is not None]
            else:
                dep_names = []
            logger.debug(
                f"id: {job_id} proc: {stage} dep: "
                f"{dep_names} dep_id: {dep_ids}"
            )

        # now we can prepare all the files
        self._prepare_files()

        # Iterate over folders, files and projects
        for folder in self.unique_basenames:
            logger.debug(f"-------> Working on folder {folder}")
            log_file = self.create_cmd_log(folder=folder)
            for f_name in self.unique_basenames[folder]:
                for stage in exec_plan["file"]:
                    logger.debug(f"\n==> At stage: [{stage}]")
                    job_counter += 1
                    father = dependencies.get_father(stage)
                    dep_ids = id_store.get_depending_ids(
                        fdr=folder, fi=f_name, fathers=father,
                        level='file'
                    )

                    conf_job = JobConfig(
                        stage_name=stage,
                        run_options=self.run_options,
                        folder=folder,
                        f_name=f_name,
                        dep_ids=dep_ids,
                        father=father,
                        test=self.config_obj.TEST,
                        job_counter=job_counter,
                        log_file=log_file,
                    )

                    job_id = dispatch_job(stage, conf_job)

                    if self.is_clean_up(stage):
                        to_remove.append(f"{conf_job.stage_name}"
                                         f"_{conf_job.folder}")

                    id_store.add_id(
                        fdr=folder, fi=f_name, st=stage, job_id=job_id,
                        level='file'
                    )
                    id_store.add_fail(job_id, stage, self.run_options)

                    if father is not None:
                        dep_names = [x.name for x in father if x is not None]
                    else:
                        dep_names = []
                    logger.debug(
                        f"id: {job_id} proc: {stage} dep: "
                        f"{dep_names} dep_id: {dep_ids}"
                    )

            for stage in exec_plan["folder"]:
                job_counter += 1

                logger.debug(f"\n==> At stage: [{stage}]")
                father = dependencies.get_father(stage)
                dep_ids = id_store.get_depending_ids(fdr=folder,
                                                     fathers=father,
                                                     level='folder')

                conf_job = JobConfig(
                    stage_name=stage,
                    run_options=self.run_options,
                    folder=folder,
                    f_name=self.unique_basenames[folder],
                    dep_ids=dep_ids,
                    father=father,
                    test=self.config_obj.TEST,
                    job_counter=job_counter,
                    log_file=log_file,
                )

                job_id = dispatch_job(stage, conf_job)

                if self.is_clean_up(stage):
                    to_remove.append(f"{conf_job.stage_name}"
                                     f"_{conf_job.folder}")

                id_store.add_id(fdr=folder, st=stage, job_id=job_id,
                                level='folder')
                id_store.add_fail(job_id, stage, self.run_options)

                logger.debug(
                    f"id: {job_id} proc: {stage} dep: "
                    f"{[x.name for x in father]} dep_id: {dep_ids}"
                )

        for stage in exec_plan['multifolder']:
            logger.debug(f"\n==> At stage: [{stage}]")
            father = dependencies.get_father(stage)
            self._find_multifolders_deps(stage=stage, father=father)
            for common_name, dep_folders in self.unique_multifolder.items():
                job_counter += 1
                dep_ids = id_store.get_depending_ids(fathers=father,
                                                     mfdr=dep_folders,
                                                     level='multifolder')
                conf_job = JobConfig(
                    stage_name=stage,
                    run_options=self.run_options,
                    folder=common_name,
                    f_name=dep_folders,
                    dep_ids=dep_ids,
                    father=father,
                    test=self.config_obj.TEST,
                    job_counter=job_counter,
                    log_file=log_file,
                )
                job_id = dispatch_job(stage, conf_job)
                id_store.add_fail(job_id, stage, self.run_options)
                id_store.add_id(fdr=common_name, st=stage, job_id=job_id,
                                level='multifolder')

        for stage in exec_plan["project"]:
            job_counter += 1

            father = dependencies.get_father(stage)
            dep_ids = id_store.get_depending_ids(fathers=father,
                                                 level='project')
            conf_job = JobConfig(
                stage_name=stage,
                run_options=self.run_options,
                folder="folder",
                f_name="",
                dep_ids=dep_ids,
                father=father,
                test=self.config_obj.TEST,
                job_counter=job_counter,
                log_file=log_file,
            )

            job_id = dispatch_job(stage, conf_job)
            id_store.add_fail(job_id, stage, self.run_options)

            if self.is_clean_up(stage):
                to_remove.append(f"{conf_job.stage_name}"
                                 f"_{conf_job.folder}")

            id_store.add_id(st=stage, job_id=job_id, level='project')

            logger.debug(
                f"id: {job_id} proc: {stage} dep: "
                f"{[x.name for x in father]} dep_id: {dep_ids}"
            )

        # finally send the job to clean up folders flagged by the user
        to_remove = set(to_remove)
        if len(to_remove):
            logger.debug("Will clean up:")
            logger.debug(f"{' '.join(to_remove)}")
            all_ids = id_store.get_all_ids()
            cleaner = SubmitCleanUp(to_remove, all_ids)
            if not self.config_obj.TEST:
                id_cleaner = cleaner.run()
                id_store.add_id(st='cleaner', job_id=id_cleaner,
                                level='project')

        logger.info("Prepare level jobs:")
        logger.info(json.dumps(id_store.prepare_id, indent=4))
        logger.info("File level jobs:")
        logger.info(json.dumps(id_store.file_id, indent=4))
        logger.info("\nFolder level jobs:")
        logger.info(json.dumps(id_store.folder_id, indent=4))
        logger.info("\nMultifolder level jobs:")
        logger.info(json.dumps(id_store.multifolder_id, indent=4))
        logger.info("\nProject level jobs:")
        logger.info(json.dumps(id_store.project_id, indent=4))

        if self.config_obj.TEST or self.config_obj.NO_DAEMON:
            return id_store
        else:
            # try to prevent weird names messing around
            enter_daemon(ids=id_store, pid_fname=pid_file_name,
                         server_name=server_name)
            return id_store


def write_out_example():
    """Write an example input file to the current path"""
    with open("example_input.ini", "w") as fo:
        fo.write(example_input)


def check_cmds_in_path(run_options: Dict):
    for st in run_options['stages']:
        if 'cmd' in run_options['stages'][st]:
            temp_cmd = run_options['stages'][st]['cmd']
            # could be that it's a cmd followed with a subcommand
            check_cmd = next(iter(temp_cmd.split()), None)
            if shutil.which(check_cmd) is None and  not pathlib.Path(check_cmd).is_file() :
                msg = f"Error: command {check_cmd} from stage {st} not in " \
                    f"the path"
                logger.error(msg)
                sys.exit()
        else:
            msg = f"Error: stage {st} input did not define a cmd option."
            logger.error(msg)
            sys.exit()
    pass


def runner_factory(config_obj: BasicConfig, *, cmd_args: Dict = None):
    """Configure and return and app.
    """

    try:
        if not config_obj.TEST:
            if cmd_args is not None:
                if cmd_args["example"]:
                    write_out_example()
                    sys.exit()
                else:
                    input_file = cmd_args["i"]
                    no_daemon = cmd_args["no_daemon"]
                    setattr(config_obj, "INPUT_FILE", input_file)
                    setattr(config_obj, "NO_DAEMON", no_daemon)
            else:
                logging.error("Did not receive any command line argument.")
                sys.exit()

    except KeyError as e:
        logger.debug("")

    run_options = parse_input_file(config_obj.INPUT_FILE, config_obj.DEBUG)
    if not config_obj.TEST:
        check_cmds_in_path(run_options)
    job_run = Runner(run_options, config_obj)

    return job_run


def start_runner(args) -> None:
    """Returns a runner from the factory.
    
    :return: None
    """
    run = runner_factory(BasicConfig, cmd_args=args)
    if run.run():
        # we don't want to bother the user after exiting the daemon
        if run.config_obj.TEST or run.config_obj.NO_DAEMON:
            print("All done, time to relax.")
    else:
        sys.exit(0)
