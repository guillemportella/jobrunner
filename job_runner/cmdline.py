import argparse
import sys
from argparse import RawDescriptionHelpFormatter

import pkg_resources
from colorama import Fore
from colorama import init

from job_runner.jobquery import DESC_QUERY, start_query
from job_runner.runner import DESC_LAUNCH
from job_runner.runner import start_runner

init()

DESC_MAIN = f"""Tools for typical genomic workflow execution in SLURM clusters.

• {Fore.BLUE}jr{Fore.RESET} {Fore.GREEN}launch{Fore.RESET}: submit a workflow.
• {Fore.BLUE}jr{Fore.RESET} {Fore.GREEN}query{Fore.RESET}: query project state.  

"""

DATE = "06-2019"
version = pkg_resources.require("job_runner")[0].version
whowhen = f"Guillem Portella, v{version} {DATE}"


def parse_cmd_line():
    """Parse the arguments."""
    # create the top-level parser
    parser = \
        argparse.ArgumentParser(description=DESC_MAIN,
                                epilog=whowhen,
                                formatter_class=RawDescriptionHelpFormatter)
    subparsers = parser.add_subparsers()

    # Args for "launch" command
    parser_launch = \
        subparsers.add_parser('launch',
                              description=DESC_LAUNCH,
                              epilog=whowhen,
                              formatter_class=RawDescriptionHelpFormatter)

    parser_launch.add_argument(
        '-nd', '--no-daemon',
        default=False,
        action='store_true',
        help='Do not start the controller daemon.'
    )
    group_launch = parser_launch.add_mutually_exclusive_group(required=True)
    group_launch.add_argument(
        "-i",
        metavar="input_file",
        nargs="?",
        help="The table containing conditions and references.",
    )
    group_launch.add_argument(
        "-example",
        action="store_true",
        help="Write out an input example file.",
    )

    # call this function if we select significant_rg4
    parser_launch.set_defaults(func=start_runner)

    # Args for "query" command
    parser_query = \
        subparsers.add_parser('query',
                              description=DESC_QUERY,
                              epilog=whowhen,
                              formatter_class=RawDescriptionHelpFormatter)

    group_query = parser_query.add_mutually_exclusive_group(required=False)
    group_query.add_argument(
        "-p",
        "--pid",
        default=False,
        action="store_true",
        help="report the PID of the process monitoring the jobs",
    )
    group_query.add_argument(
        "-k",
        "--stop_jobs",
        default=False,
        action="store_true",
        help="stop all the jobs for this project",
    )
    group_query.add_argument(
        "-m",
        "--missing",
        default=False,
        action="store_true",
        help="only print out the jobs that are running or pending",
    )
    group_query.add_argument(
        "-kns",
        "--stop_name_server",
        default=False,
        action="store_true",
        help="stop the server that interfaces with the monitoring jobs. "
             "(the jobs will keep running, but you won't be able to interact "
             "with them)",
    )

    # call this function if we select significant_rg4
    parser_query.set_defaults(func=start_query)

    # parse the args and call whatever function was selected
    if not len(sys.argv) > 1:
        parser.parse_args(['--help'])

    args = parser.parse_args()
    args.func(vars(args))
