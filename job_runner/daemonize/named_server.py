"""Start monitoring of the jobs just submitted.

We leverage Pyro4 to create a monitoring system for the jobs, such that
we interact with these objects to obtain the status of the executions for that
particular job: we are able to query they status and kill them.

For convenience, pyro4 has a name server (NS) that acts like a telephone book.
We can talk to it to get the address of the object/monitor that we require.
Since each user will have their own NS running in the same localhost, we
differentiate the NS for each user based on port number. The port number
is established based on the user name, for lack of a better mechanism. I don't
expect collisions, but it's theoretically possible.

The main tools is function is pyro4_start(), in it we:

initially come up deterministically with a port number for the user. With that
as a initial start point, we scan ports to check if the user has already a
name server running. If so, we get the port number. If not, we look for the
port number that is available and closest to the initial start point. We
then start the NS server, and we attach a pyro4 object that will help us
interact with the jobs.
After registering the controlling object, we then start an infinite
loop that monitors the jobs. The loop breaks when all the jobs have finished.
If a job fails, we kill the rest of the project. The monitoring loop will not
see jobs in the queue associated with the project and will hence break out
of the infinite loop.

At each possible breaking point we try to remove the object from the NS
not to create orphaned jobs.


"""
import getpass
import logging
import os
import socket
import subprocess
import time
from collections import namedtuple
from contextlib import contextmanager
from functools import partial
from typing import Dict

import Pyro4
import fyrd
from Pyro4 import Daemon

from job_runner.daemonize import pyro4_utils as p_utils
from job_runner.planner.job_planner import StoreIds

logger = logging.getLogger("runner_logger")


@Pyro4.expose
class EmbeddedServer(object):
    """Utilities that we can interact with via the server.

    The class needs to be instantiated with the daemon, the jobs and
    the server name in order to work because we need this information
    to stop the jobs, etc.

    The server name is used throughout the code as the object name that
    appears in the pyro4 name server.
    """

    def __init__(self, daemon: Daemon, sub_jobs: StoreIds, server_name: str):
        """Store the daemon and the jobs
        :param server_name: str: name of the object
        :param daemon: Daemon: instance of the pyro4 daemon
        :param sub_jobs: StoreIds: class holding the jobs and their ids
        """
        self.daemon = daemon
        self.sub_jobs = sub_jobs
        self.server_name = server_name

    def job_status(self):
        """Return a dictionary {job_ids: NamedTuple(state, name)}"""
        return jobs_q(self.sub_jobs)

    @staticmethod
    def get_pid():
        """Return the PID of jobrunner, i.e. the pyro4 daemon"""
        return os.getpid()

    @Pyro4.oneway
    def pyro4_shutdown(self):
        """Stop the pyro4 daemon.

        The jobs will continue running in the server
        We will induce an exception in daemon, but first we should try
        to remove the object from the name server
        """
        try:
            p_utils.remove_from_ns(self.server_name, self.port)
        except (
                ConnectionRefusedError,
                Pyro4.errors.NamingError,
                Pyro4.errors.ConnectionClosedError,
        ) as _:
            logger.warn("Can not remove from server. Shut down anyway.")
        self.daemon.shutdown()

    @Pyro4.oneway
    def stop_jobs(self):
        """Kill all the running jobs for the whole project.

        The pyro4 server should stop automatically as it detects that there
        are not more jobs running. The monitoring loop should exit
        cleanly, and afther that the object could be removed from the
        name server
        """
        kill_project(self.sub_jobs)

    @staticmethod
    def status():
        return True


def job_kill(job_id: int) -> bool:
    """Kill a job given its slurm pid
    :param job_id: int: pid of the job to kill
    :return: bool: false if passed the wrong type
    """

    if isinstance(job_id, int):
        sbatch_cmd = "scancel " + str(job_id)
        subprocess.call(sbatch_cmd, shell=True)
    else:
        logger.error("job_id should be an integer!")
        return False
    return True


StateName = namedtuple("StateName", 'state name')


def jobs_q(jobs_proj: StoreIds) -> Dict[int, StateName]:
    """Return dict of job states the cluster for a given project.

    :param jobs_proj: StoreIds:  class storing the ids at for each stage/job
    :return: Dict[int, QueueJob]
    """
    q = fyrd.Queue()
    user = getpass.getuser()
    my_jobs = q.get_user_jobs([user])
    current = {}
    jobs_ids = jobs_proj.get_all_ids()
    for iid, w in my_jobs.items():
        # the job id in fyrd is actually str...
        if int(iid) in jobs_ids:
            current[int(iid)] = StateName(w.state, w.name)
    return current


def kill_project(sub_jobs: StoreIds) -> bool:
    """Kill all the active jobs for a given project.

    The job dependencies will be killed by the queuing system,
    due to the "kill-on-invalid-dep" -- unless you've monkey patched it --
    and certain job ids won't be there any more as we iterate.
    This should be fine, as we don't care about the return of the
    job_kill subprocess.

    :return: True, not really catching exceptions.
    :param sub_jobs: StoreIds:  class storing the ids at for each stage/job
    """
    my_jobs = jobs_q(sub_jobs)

    for iid, j in my_jobs.items():
        if j.state == "pending" or j.state == "running":
            job_kill(iid)

    return True


def monitor(sub_jobs: StoreIds) -> bool:
    """ Follows the execution of the jobs.

    It will kill all the jobs within the whole pipeline if it detects that
    a single job has failed. Unless... it has the "can_fail" option set
    to True. However, if the failed job has jobs that depend on it, then
    these will be killed by the queueing system, and unless they have the
    "can_fail" to True, they will make the whole pipeline stop.

    If the number of pending jobs is zero, it will return False, else True.

    :param sub_jobs: StoreIds:  class storing the ids at for each stage/job
    :return:bool
    """
    my_jobs = jobs_q(sub_jobs)
    kill_all = False
    pending = False

    for iid, j in my_jobs.items():
        if j.state == "pending" or j.state == "running":
            pending = True
            # pending.append(int(iid))
        if j.state == "failed":
            # only kill all the rest if can_fail flag is set to False
            # please not that even if can_fail is True, the pipeline will be
            # stopped if this stage has dependents that can not fail, because
            # the queueing system will see that the father died and stop the
            # dependents, triggering that we stop all the jobs. This is
            # is actually the kind of behaviour we want.
            try:
                can_fail = sub_jobs.id_can_fail[iid]
            except KeyError as e:
                msg = f"Can not find job_id {iid} in stored can_fail ids. " \
                    f"We will assume it can not fail."
                logger.warning(msg)
                kill_all = True
            else:
                if not can_fail:
                    kill_all = True

    if kill_all:
        kill_project(sub_jobs)

    if pending:
        return True
    else:
        return False


@contextmanager
def pyro4_start(server_name: str, jobs: StoreIds):
    """Context manager for starting a pyro4 daemon

    We use a pyro4 name server to locate the objects. If there is none running,
    we subprocess one. We then enter an infinite loop that can be
    broken by:

    1. all jobs having finished (either killed or naturally)
    2. the EmbeddedServer (our pyro object) receives a shutdown command
           that stops the daemon. This will cause and exception, which
           will then yield and we can leave the contextmanager


    :param server_name: str: the name of pyro object server
    :param jobs: StoreIds:  class storing the ids at for each stage/job
    :param server_type: str: 't' for threaded, anything else is multiplex
    """

    # get a port for the server
    logger.debug("Get the ns port..")
    # Pyro4.config.SERVERTYPE = "thread"
    Pyro4.config.SERVERTYPE = "multiplex"
    try:
        # if there is already a NS, get it's port
        logger.debug("Try to get a ns port")
        ns_port = p_utils.scan_ns(port=p_utils.select_port())

    except (OSError, Pyro4.errors.NamingError) as _:
        logger.debug(" Cound not find a NS port for the user, try free one.")
        ns_port = p_utils.get_free_port(init_port=p_utils.select_port())
        logger.debug(f"Ok, got one: {ns_port}")

    # start the server
    try:

        hostname = socket.gethostname()
        pyrodaemon = Pyro4.Daemon(host=hostname)

        logger.debug("Starting...")

        # check if there is already a name server running, init monitoring loop
        try:

            port = p_utils.scan_ns(port=ns_port)
            logger.debug(f"Trying with port {port}")
            ns = Pyro4.locateNS(host="localhost", port=port)

            # if not then launch a new one and start local monitoring loop
        except (OSError, Pyro4.errors.NamingError):

            logger.debug(f"Try to start with new port: {ns_port}")
            try:
                # start a new name server
                logger.debug(f"Start new NS with port {ns_port}")
                p_utils.init_ns(ns_port)
                time.sleep(5)
                ns = Pyro4.locateNS(host="localhost", port=ns_port)

            except (ConnectionRefusedError,
                    Pyro4.errors.NamingError,
                    Pyro4.errors.ConnectionClosedError) as e:
                logger.error("Something is off while trying to launch ns")
                logger.error(e)
                yield "Noope"

            else:
                my_embedded = EmbeddedServer(pyrodaemon, jobs, server_name)
                uri = pyrodaemon.register(my_embedded)
                ns.register(server_name, uri)
                pyrodaemon.requestLoop(partial(monitor, jobs))
                # de-register object from the name server
                ns.remove(server_name, uri)
                pyrodaemon.close()
                yield "Done"

        else:
            my_embedded = EmbeddedServer(pyrodaemon, jobs, server_name)
            uri = pyrodaemon.register(my_embedded)
            ns.register(server_name, uri)
            pyrodaemon.requestLoop(partial(monitor, jobs))
            # de-register object from the name server
            ns.remove(server_name, uri)
            pyrodaemon.close()
            yield "Done"

    except (
            ConnectionRefusedError,
            Pyro4.errors.NamingError,
            Pyro4.errors.ConnectionClosedError,
    ) as e:
        logger.error("Could not start the monitoring system...")
        logger.error(e)
        yield "Stopped/killed"
