"""Start a daemon monitoring the jobs.

The function enter_daemon is responsible to fork and remain in the background.
In the background it will start the monitoring tools. Once all the jobs
have finished/killed, the daemon will simply exit.

We only allow one instance of the Daemon by means of a pid file. When it
starts, the daemon locks the file, and it removes it when it leaves.
Before sending the actual jobs to the cluster (in runner.py) it's a good
idea to check if the locked PID file is there. This avoids havina bunch of
jobs run without the daemon.

The pid file depends on the name of the project, name of the run and name
of the file. If you change this, you will still be able to submit the jobs.
If you manually remove the PID file, you will also be able to submit the jobs
more than once.

"""
import logging
import os
import pathlib
import signal
import sys

import daemon
from pid import PidFile, PidFileAlreadyLockedError

from job_runner.daemonize.named_server import pyro4_start
from job_runner.planner.job_planner import StoreIds

logger = logging.getLogger("runner_logger")

PID_FOLDER = "jobrunner_pids"
MONITOR_SERVER_NAME_FNAME = "monitor_server_name.txt"


def shutdown(signum, frame):  # signum and frame are mandatory
    sys.exit(0)


def enter_daemon(ids: StoreIds = None, pid_fname: str = "foo", *,
                 server_name: str = "pyro4_server") -> None:
    """Start the daemon and the pyro4 server.

    Uses python-daemon to put the program running in the background. Once
    in it, it starts the pyro4 server, which is the one responsible for
    keeping the loop alive.

    :param pid_fname: str: the name of the pid file
    :param ids: StoreIds: class storing the ids at for each stage/job
    :param server_name: str: name of the server, you need to know it in order to
    be able to connect to it
    """

    # it seems that if you are running in an NFS partitions PidFile
    # has troubles locking the pid file.
    home_pids = pathlib.Path.home() / PID_FOLDER
    pathlib.Path(home_pids).mkdir(exist_ok=True)

    # this has to be done before entering the daemon
    try:
        with open(MONITOR_SERVER_NAME_FNAME, "w") as fo:
            print(f"{server_name}", file=fo)
    except IOError as e:
        logger.warning(
            f"could not write out the name of the server to "
            f"{MONITOR_SERVER_NAME_FNAME}")

    try:
        with daemon.DaemonContext(
                signal_map={signal.SIGTERM: shutdown,
                            signal.SIGTSTP: shutdown},
                stdout=sys.stdout,  # just for testing now
                stderr=sys.stderr,  # just for testing now
                files_preserve=[logger.handlers[0].stream.fileno()],
                pidfile=PidFile(pid_fname, piddir=home_pids)
        ):
            logger.info(
                f"Starting background control -- PID {os.getpid()}\n"
                f"Monitoring server name: {server_name}\n"
                "Press any key to continue if prompt does not return."
            )

            with pyro4_start(server_name, jobs=ids) as serv:
                pass

    except PidFileAlreadyLockedError:
        print("It appears you have an instance of the server running.")
        sys.exit()
