"""Assorted tools used in to interact/create pyro4 name servers and objects.
"""
import getpass
import hashlib
import logging
import random
import socket
import subprocess
from typing import Union

import Pyro4
from Pyro4 import naming

logger = logging.getLogger("runner_logger")

MAX_PORT_ATTEMPTS = 50


def init_ns(port) -> None:
    """Start a pyro4 name server at a given port.

    It's a simple subprocess run, without error or return checking.
    You are then responsible to check if the server actually started.
    """

    cmd = f"nohup pyro4-ns -p {port} &> /dev/null &"
    subprocess.run(cmd, shell=True)


def select_port() -> int:
    """Select a port based on the user name

    We would like to obtain a port number in the range 10000:65535
    that is unique for each user and can be replicated every time.
    All the information we have is the user name.

    Creates a hash from the username, and then converts this hash
    into a integer, which is used as random seed to select a number
    from the run. Another option would be to convert the user name
    to an integer directly, without hashing.

    :rtype: int
    """

    user = getpass.getuser()
    m = hashlib.sha1(bytes(user, encoding="utf-8"))
    seed = sum([ord(c) for c in str(m.hexdigest())])
    random.seed(seed)
    port = random.randint(10000, 65535)
    return port


def remove_from_ns(registered_name: str, port: int) -> None:
    """Remove a pyro4 object from the name server.

    The pyro4 object must be referred to by name.
    :param registered_name: str: name of the pyro4 object
    :param port: int: port number where the NS is running
    :raises: ConnectionRefusedError, Pyro4.errors.NamingError,
            Pyro4.errors.ConnectionClosedError,
    """
    try:
        ns = Pyro4.locateNS(port=port)
        logging.debug(f"Removing {registered_name} from name server")
        ns.remove(registered_name)
    except (
            ConnectionRefusedError,
            Pyro4.errors.NamingError,
            Pyro4.errors.ConnectionClosedError,
    ) as _:
        msg = "I'm afraid there is no NS anymore."
        logging.error(msg)
        raise


def get_free_port(init_port=9090) -> int:
    """Get the bindable port number closest to a given one.

    :param init_port: int: start looking for open port at this port number
    :return: int: port number
    :raises: EnvironmentError
     """

    at = 0
    while at <= MAX_PORT_ATTEMPTS:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            try:
                s.bind(("127.0.0.1", init_port))
                return init_port
            except socket.error as _:
                # we don't really care which type of error we get, we move on
                init_port += 1
        at += 1
    else:
        raise EnvironmentError("Could not find a free port to use.")


def scan_ns(port=9090) -> Union[Pyro4.errors.NamingError, int]:
    """Look for the port number of the user's name server.

    Starting at a given port, try to connect to a name server.
    If the server is not there, keep increasing the port number until
    hopefully we find one after MAX_PORT_ATTEMPTS

    :raises: Pyro4.errors.NamingError
    :param port: int: port number to start searching from
    :return: Union[Pyro4.errors.NamingError, int]:
    :raises: Pyro4.errors.NamingError

    """
    at = 0
    while at <= MAX_PORT_ATTEMPTS:
        try:
            _ = naming.locateNS(host="localhost", port=port)
            return port
        except (
                ConnectionRefusedError,
                Pyro4.errors.NamingError,
                Pyro4.errors.ConnectionClosedError,
        ) as _:
            port += 1
        at += 1
    else:
        raise Pyro4.errors.NamingError("No name server found")
