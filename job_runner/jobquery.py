"""Interact with the main pyro4 monitoring system.

We talk to the monitoring objects for each jobs using the name server that
each user has started. We reach the name server by its port number using the
same heuristics as we used to start it. If nothing is available we say so
and we quit.

By default we return the status of the jobs associated with a project.
If there is more than one project, you will be prompted to select one.
We can use the tools to kill all the jobs of a given projects, or to kill the
name server. If we kill the name server we won't be able to talk to the
daemons monitoring the jobs, but the jobs will keep running.

"""
import functools
import logging
import subprocess
import sys
from typing import Tuple, List

import Pyro4
from Pyro4 import naming, errors
from colorama import Fore, Style
from colorama import init

from job_runner.daemonize import pyro4_utils as p_utils

logger = logging.getLogger("runner_logger")

# colorama initializations
init()

DESC_QUERY = """
Query the project running in the cluster.

By default we return the status of the jobs associated with a project.
If there is more than one project, you will be prompted to select one.

Use the -p switch to ge the monitoring job PID for a project.
Use -k switch to stop all the jobs for a project.
Use -kns switch to stop the server that interfaces with the monitoring jobs.

"""


def get_pid_pyro() -> List[int]:
    """Return the list of PIDs of the pyro4 name server for a user.

    Return and empty list if nothing has been found.
    :return: List[int]: list of PIDs of pyro4 name server. Empty if none found
    """
    ps = subprocess.Popen(('ps', '-A'), stdin=subprocess.PIPE,
                          stdout=subprocess.PIPE)
    ps.wait()
    ret = []
    try:
        gr = subprocess.check_output(('grep', 'pyro4-ns'), stdin=ps.stdout)
    except subprocess.CalledProcessError as _:
        return ret
    else:
        for p in gr.decode().split("\n"):
            if len(p):
                ret.append(int(next(iter(p.split()))))
        return ret


def get_user_input(choose):
    """Return a valid input for the registered object."""
    try:
        chosen = int(input())
        while int(chosen) not in choose:
            print("Sorry, option not in the list. Ctrl+C to quit.")
            return get_user_input(choose)
    except ValueError as _:
        print(f"Please, feed me an integer between 0 and {len(choose)-1}")
        return get_user_input(choose)
    return choose[chosen]


def proxy_connect(server_name: str, port: int):
    """Decorates to get a proxy and connect if possible.

    Opens up a proxy and passes that proxy to the function as argument.
    This is done inside a try/except to fail gracefully if the proxy
    can not connect.

    :param server_name: str: server name
    :param port: int: port number
    :return: function
    """

    def wrap(f):
        functools.wraps(f)

        def wrapped_f(*args):
            with Pyro4.core.Proxy(
                    f"PYRONAME:{server_name}@localhost:{port}") as proxy:
                try:
                    f(*args, proxy=proxy)
                except (
                        ConnectionRefusedError,
                        Pyro4.errors.NamingError,
                        Pyro4.errors.ConnectionClosedError,
                ) as e:
                    print("No connection to remote server.")
                    sys.exit()

        return wrapped_f

    return wrap


class QueryPyro4(object):
    """Query pyro4 server running jobrunner daemon."""

    def __init__(self, server_name: str = None, port: int = 9090):
        self.server_name = server_name
        self.port = port

    @staticmethod
    def print_status(r: str, n: str, s: str):
        """ Print the status of all the jobs in the project."""
        if s == "pending":
            print(f"JobId: {Fore.CYAN}{r}{Fore.RESET} "
                  f"Name: {Fore.MAGENTA}{n}{Fore.RESET} "
                  f"Status: {Fore.YELLOW}{s}{Style.RESET_ALL}")

        elif s == "running":
            print(f"JobId: {Fore.CYAN}{r}{Fore.RESET} "
                  f"Name: {Fore.MAGENTA}{n}{Fore.RESET} "
                  f"Status: {Fore.WHITE}{s}{Style.RESET_ALL}")

        elif s == "failed" or s == "cancelled":
            print(f"JobId: {Fore.CYAN}{r}{Fore.RESET} "
                  f"Name: {Fore.MAGENTA}{n}{Fore.RESET} "
                  f"Status: {Fore.RED}{s}{Style.RESET_ALL}")

        elif s == "completed":
            print(f"JobId: {Fore.CYAN}{r}{Fore.RESET} "
                  f"Name: {Fore.MAGENTA}{n}{Fore.RESET} "
                  f"Status: {Fore.GREEN}{s}{Style.RESET_ALL}")

        else:
            print(f"JobId: {Fore.CYAN}{r}{Fore.RESET} "
                  f"Name: {Fore.MAGENTA}{n}{Fore.RESET} "
                  f"Status: {Fore.BLUE}{s}{Style.RESET_ALL}")

    def status(self, *, missing: bool = False):
        """Return the job status for a given server.

        If missing is set to true, only report the ones that are
        either running or pending.
        """

        @proxy_connect(self.server_name, self.port)
        def print_jobs(proxy=None):
            results = proxy.job_status()
            print()
            for r, j in results.items():
                state, name = j
                if len(name) > 20:
                    print_name = name[:20] + '[...]'
                else:
                    print_name = name
                if missing:
                    if state == 'running' or state == 'pending':
                        self.print_status(r, print_name, state)
                else:
                    self.print_status(r, print_name, state)

        print_jobs()

    def stop_jobs(self):
        """Stop all the jobs in given server"""

        @proxy_connect(self.server_name, self.port)
        def stop(proxy=None):
            proxy.stop_jobs()

        stop()

    def get_runner_pid(self):
        """Return the PID of the project jobrunner"""

        @proxy_connect(self.server_name, self.port)
        def get_pid(proxy=None):
            pid = proxy.get_pid()
            print(f"The jobrunner PID is {pid}")

        get_pid()


def kill_ns():
    """Stop the jobrunner name server.

    We assess if it's actually working, and then we try to grep the
    pid of a pyro4-ns process running for the user, and send a kill -9
    to that pid.
    """
    pid = get_pid_pyro()
    logger.debug(f"PID NS to kill is {pid}")
    if pid:
        what = " ".join([str(i) for i in pid])
        subprocess.call(f"kill -9 {what}", shell=True)
    else:
        print("No name server found...")
        sys.exit()


def select_project() -> Tuple[str, int]:
    """Return the name of the pyro4 object in the name server.

    We locate the port of the name server with the same tools as we use to
    allocate it. If the user has only one registered object, we simply use
    that one. If there are more than one, we present a menu to the user.

    :return: Tuple[str, int]: tuple with the project in NS and the port of NS
    """

    try:
        free_port = p_utils.scan_ns(port=p_utils.select_port())
        ns = naming.locateNS(host="localhost", port=free_port)
        jobs = [k for k, v in ns.list().items() if k != "Pyro.NameServer"]
        n_jobs = len(jobs)
        if n_jobs == 1:
            the_one = next(iter(jobs))
            print(f"\nYou have just one project, {the_one}")
            return the_one, free_port
        elif n_jobs > 1:
            print("Choose one: ")
            choose = {}
            for i, n in enumerate(jobs):
                choose[i] = n
                print(f"{i} - {n}")
            return get_user_input(choose), free_port
        else:
            logger.info("There are no registered jobs running.")
            sys.exit(0)

    except (
            ConnectionRefusedError,
            Pyro4.errors.NamingError,
            Pyro4.errors.ConnectionClosedError,
    ) as e:
        logger.info(f"No connection to remote server ")
        sys.exit()


def start_query(args) -> None:
    """Returns a runner from the factory.

    :return: None
    """
    if args['stop_name_server']:
        kill_ns()
    else:
        pyro4_name, port = select_project()
        pyro4_server = QueryPyro4(server_name=pyro4_name, port=port)
        if args["stop_jobs"]:
            pyro4_server.stop_jobs()
        elif args['pid']:
            pyro4_server.get_runner_pid()
        elif args['missing']:
            pyro4_server.status(missing=True)
        else:
            pyro4_server.status()
