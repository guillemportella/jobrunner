.. _my_installation:

Installation
=============

.. warning:: *Legacy* python not supported, only ``python >= 3.6``. Make
    you are also using python3 to install it via ``pip``.

As a traditional package
**************************

Install for your user like,

.. code-block:: bash

    git clone git@gitlab.com:guillemportella/jobrunner.git
    cd jobrunner
    # make sure pip is the python3 pip
    pip install . --user

Then executable ``jr`` should be in your ``PATH`` and
will execute the program.

In a virtual environment
*************************

Using ``pipenv`` (``pip install pipenv`` if not installed).
To easily run the app in a well-defined environment, avoid contaminating
your python path with funny dependencies, specially during development.

.. code-block:: bash

    git clone git@gitlab.com:guillemportella/jobrunner.git
    cd jobrunner
    pipenv install --dev

You can then run it like,

.. code-block:: bash

    pipenv run ./jr

.. _test_reference_label:

Tests
******

Right now they do not test the submission to ``SLURM``, but most of the input preparation.

Without virtual environment
-------------------------------

In the ``jobrunner`` folder, execute

.. code-block:: bash

    python -m pytest -v -s

In a virtual environment
---------------------------

In the ``jobrunner`` folder, execute

.. code-block:: bash

    pipenv run python -m pytest -v -s


