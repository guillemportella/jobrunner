import pytest

from job_runner.parse.parser import parse_input_file, parse_regex
from job_runner.parse.parser import parse_option


def test_parse_chipseq(config_parser_chipseq):
    """ System exits when the wrong options are present
    GIVEN a configuration parser with parsing object loaded in
    WHEN parsing chipseq options
    THEN do the right thing
     """
    rt = parse_option(config=config_parser_chipseq, field='chipseq')

    should_be = {'cmd': 'macs2 callpeak', 'paired_control': 'no',
                 'control': '_Input', 'enrich': '_ChIP*',
                 'multifolder_postfix': '_Input _ChIP*',
                 'depends': 'filterduplicates',
                 'memory': '60G',
                 'dependency_type': 'multifolder', 'can_fail': 'False'}
    assert rt == should_be


def test_required_option_multidep(config_parser_multidep):
    """ System exits when the wrong options are present
    GIVEN a configuration parser with parsing object loaded in
    WHEN parsing option to consume later on
    THEN return either exit or a dict with options
     """
    with pytest.raises(SystemExit):
        parse_option(config=config_parser_multidep, field='trimming',
                     required=['fake'])

    with pytest.raises(SystemExit):
        parse_option(config=config_parser_multidep, field='peeling')

    rt = parse_option(config=config_parser_multidep, field='multiqc',
                      required=['cmd'])
    assert type(rt) is dict
    test_dict = dict(cmd='multiqc', depends='quality preseq',
                     dependency_type='project', can_fail='False',
                     memory='60G')
    assert test_dict == rt


def test_required_option(config_parser):
    """ System exits when the wrong options are present
    GIVEN a configuration parser with parsing object loaded in
    WHEN parsing option to consume later on
    THEN return either exit or a dict with options
     """
    with pytest.raises(SystemExit):
        parse_option(config=config_parser, field='trimming', required=['fake'])

    with pytest.raises(SystemExit):
        parse_option(config=config_parser, field='peeling')


def test_parsing_regex(app, config_parser):
    """ We are parsing correctly the configs
    GIVEN a configuration parser with parsing object loaded in
    WHEN parsing option to consume later on
    THEN return dict with a regex in it
     """
    example_regex = app['example_regex']
    rt = parse_regex(config_parser)
    assert rt == example_regex


def test_parsing_dict(config_parser):
    """ We are parsing correctly the configs
    GIVEN a configuration parser with parsing object loaded in
    WHEN parsing option to consume later on
    THEN return the right dictionary
     """
    rt = parse_option(config=config_parser, field='trimming_galore',
                      required=['cmd'])
    assert type(rt) is dict
    test_dict = {'can_fail': 'False', 'memory': '60G', 'cmd': 'trim_galore',
                 'quality': '10',
                 'depends': 'bcl2fastq', 'dependency_type': 'file',
                 'gzip': 'True', 'stringency': '8'}
    assert test_dict == rt


def test_conflicting_input(config_parser_conflict):
    """Test that we actually stop if we put two conflicting options"""
    with pytest.raises(SystemExit):
        rt = parse_option(config=config_parser_conflict, field='align',
                          required=['cmd', 'required'])


def test_returns_dict(app):
    """We are actually returning a dict when parsing the input"""
    runner = app['runner']
    rt = parse_input_file(runner.config_obj.INPUT_FILE,
                          runner.config_obj.DEBUG)
    assert type(rt) is dict


def test_reads_can_fail(config_parser_can_fail):
    rt = parse_option(config=config_parser_can_fail, field='trimming_galore',
                      required=['cmd'])
    assert type(rt) is dict
    test_dict = {'cmd': 'trim_galore', 'quality': '10', 'depends': 'bcl2fastq',
                 'dependency_type': 'file', 'gzip': 'True', 'stringency': '8',
                 'can_fail': 'False', 'memory': '60G'}
    assert test_dict == rt


def test_reads_memory(config_parser_memory):
    rt = parse_option(config=config_parser_memory, field='trimming_galore',
                      required=['cmd'])
    assert type(rt) is dict
    test_dict = {'cmd': 'trim_galore', 'quality': '10', 'depends': 'bcl2fastq',
                 'dependency_type': 'file', 'gzip': 'True', 'stringency': '8',
                 'can_fail': 'False', 'memory': '30G'}
    assert test_dict == rt
