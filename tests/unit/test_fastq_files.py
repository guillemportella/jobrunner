import pytest


def test_find_se_files(app_se):
    """ Regex parsing of file names works
    GIVEN a configuration parser with parsing object loaded in
    WHEN parsing option to consume later on
    THEN the class populates dicts with unique basenames
     """
    runner = app_se['runner']
    example_files = app_se['example_files']
    example_folder = app_se['example_folder']
    example_unique = app_se['example_unique']

    runner._find_files()
    assert type(runner.found_files) is dict
    assert len(runner.found_files[example_folder]) == len(example_files)

    runner._find_unique_files()
    assert type(runner.unique_basenames) is dict
    assert runner.unique_basenames[example_folder] == example_unique


def test_find_files(app):
    """ Regex parsing of file names works
    GIVEN a configuration parser with parsing object loaded in
    WHEN parsing option to consume later on
    THEN the class populates dicts with unique basenames
     """
    runner = app['runner']
    example_files = app['example_files']
    example_folder = app['example_folder']
    example_unique = app['example_unique']

    runner._find_files()
    assert type(runner.found_files) is dict
    assert len(runner.found_files[example_folder]) == len(example_files)

    runner._find_unique_files()
    assert type(runner.unique_basenames) is dict
    assert runner.unique_basenames[example_folder] == example_unique


def test_prepare_se_files(app_se):
    """ Regex parsing of file names works
    GIVEN a configuration parser with parsing object loaded in
    WHEN populating the class after parsing option to consume later on
    THEN class has the right files
     """
    runner = app_se['runner']
    example_files = app_se['example_files']
    example_folder = app_se['example_folder']
    example_unique = app_se['example_unique']

    runner._prepare_files()
    assert len(runner.found_files[example_folder]) == len(example_files)
    assert runner.unique_basenames[example_folder] == example_unique


def test_prepare_files(app):
    """ Regex parsing of file names works
    GIVEN a configuration parser with parsing object loaded in
    WHEN populating the class after parsing option to consume later on
    THEN class has the right files
     """
    runner = app['runner']
    example_files = app['example_files']
    example_folder = app['example_folder']
    example_unique = app['example_unique']

    runner._prepare_files()
    assert len(runner.found_files[example_folder]) == len(example_files)
    assert runner.unique_basenames[example_folder] == example_unique


def test_wrong_parity(app_wrong):
    """ Regex parsing of file names works
    GIVEN a configuration parser with parsing object loaded in
    WHEN parsing option to consume later on
    THEN we exit if we find an even number of files to paired-end seq
     """
    runner = app_wrong['runner']

    with pytest.raises(SystemExit):
        runner._find_files()
