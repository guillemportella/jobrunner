import os


def test_log_files(app):
    runner = app['runner']
    logfile = getattr(runner.config_obj, 'LOG_FILE')
    assert os.path.exists(logfile)
