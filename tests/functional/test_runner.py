import logging

from job_runner.config import config_logging

logging.config.dictConfig(config_logging.LOG_SETTINGS)
logger = logging.getLogger('runner_logger')


def test_chipseq(runner_app_chipseq):
    """Test chipseq
    GIVEN a configuration parser with parsing object loaded in
    WHEN [chipseq] is part of the input
    THEN make sure we get the folder names right
    """
    runner = runner_app_chipseq['runner']
    ids = runner.run()
    # assert ids == False


def test_cmd_runner(runner_app, caplog):
    """ We are parsing correctly the configs
    GIVEN a configuration parser with parsing object loaded in
    WHEN getting ready to submit everything
    THEN make sure cmd submission makes sense
     """

    runner = runner_app['runner']
    id_store = runner.run()

    # this one is an "or" because sometimes we process the files
    # in different order
    assert id_store.file_id['control_rep_2'] == {
        'control_rep_2_L002': {'trimming_galore': 1, 'align_bwa': 2},
        'control_rep_2_L001': {'trimming_galore': 3, 'align_bwa': 4}} or \
           id_store.file_id[
               'control_rep_2'] == {
               'control_rep_2_L002': {'trimming_galore': 1, 'align_bwa': 2},
               'control_rep_2_L001': {'trimming_galore': 3, 'align_bwa': 4}}

    # assert True == False


def test_cmd_runner_cleaner(runner_app_cleaner, caplog):
    """ We are parsing correctly the configs
    GIVEN a configuration parser with parsing object loaded in
    WHEN getting ready to submit everything
    THEN make sure cmd submission makes sense
     """

    runner = runner_app_cleaner['runner']
    id_store = runner.run()

    # this one is an "or" because sometimes we process the files
    # in different order
    assert id_store.file_id['control_rep_2'] == {
        'control_rep_2_L002': {'trimming_galore': 1, 'align_bwa': 2},
        'control_rep_2_L001': {'trimming_galore': 3, 'align_bwa': 4}} or \
           id_store.file_id[
               'control_rep_2'] == {
               'control_rep_2_L002': {'trimming_galore': 1, 'align_bwa': 2},
               'control_rep_2_L001': {'trimming_galore': 3, 'align_bwa': 4}}


# These are just to get the code to run in debug mode, not to test anything
#
def test_can_fail(app_can_fail):
    runner = app_can_fail['runner']
    id_store = runner.run()
    pass


def test_memory_read(app_memory):
    runner = app_memory['runner']
    id_store = runner.run()
    pass
