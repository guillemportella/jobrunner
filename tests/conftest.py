import configparser
import errno
import os
import shutil
import tempfile

import pytest

from job_runner.config import BasicConfig
from job_runner.runner import runner_factory

test_input_conflict = b"""[input_type]
paired_end = True

[project]
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'

[trimming_galore]
cmd = trim_galore
Quality = 10
gzip = True
depends = None
dependency_type = file

[align_bwa]
cmd = bwa mem
reference = /scratcha/sblab/portel01/reference_data/genomes/mus_musculus/mm9/Sequence/BWAIndex/genome.fa
M = True
t = 8
short_options = True
keyvalue_options = True
depends = trimming_galore
dependency_type = file

[merge]
cmd = samtools merge
depends = align_bwa
dependency_type = folder
"""

test_input_old = b"""[input_type]
paired_end = True

[project]
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'

[trimming_galore]
cmd = trim_galore
quality = 10
stringency = 8
gzip = True
depends = None
dependency_type = file

[align_bwa]
cmd = bwa mem
reference = /scratcha/sblab/portel01/reference_data/genomes/mus_musculus/mm9/Sequence/BWAIndex/genome.fa
M = True
t = 8
short_options = True
depends = trimming_galore
dependency_type = file

[mergebam]
cmd = samtools merge 
depends = align_bwa
dependency_type = folder

[filterduplicates]
cmd = /home/martin03/sw/picard/picard-2.8.2/picard-2.8.2.jar
whitelist = /scratcha/sblab/portel01/project/euni_5fc/annotations/cpgi/mm9-whitelist.bed
sam_include = 3
sam_exclude = 3840
depends = mergebam
dependency_type = folder

[quality]
cmd = fastqc
depends = trimming_galore
dependency_type = folder

[multiqc]
cmd = multiqc
depends = quality
dependency_type = project

[submit]
cmd = submit_report.py
depends = multiqc
dependency_type = project

[folder_names]
control_rep_2 = control_rep_2

[extension_regex]
regex = _L???_R?_???.fastq.gz
no_lane = _R?_???.fastq.gz
extension = .fastq.gz
read_one = _R1_001
read_two = _R2_001
"""

test_input_memory_stage = b"""[input_type]
paired_end = True

[project]
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'

[bcl2fastq]
cmd = bcl2fastq
input_cvs = file.csv
depends = None
dependency_type = prepare


[trimming_galore]
cmd = trim_galore
quality = 10
stringency = 8
gzip = True
depends = bcl2fastq
dependency_type = file
memory = 30G

[align_bwa]
cmd = bwa mem
reference = path
M = True
t = 8
short_options = True
depends = trimming_galore
dependency_type = file
memory = 35G

[folder_names]
control_rep_2 = control_rep_2

[extension_regex]
regex = _L???_R?_???.fastq.gz
no_lane = _R?_???.fastq.gz
extension = .fastq.gz
read_one = _R1_001
read_two = _R2_001
"""

test_input_can_fail = b"""[input_type]
paired_end = True

[project]
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'

[bcl2fastq]
cmd = bcl2fastq
input_cvs = file.csv
depends = None
dependency_type = prepare

[trimming_galore]
cmd = trim_galore
quality = 10
stringency = 8
gzip = True
depends = bcl2fastq
dependency_type = file
can_fail = False

[align_bwa]
cmd = bwa mem
reference = path
M = True
t = 8
short_options = True
depends = trimming_galore
dependency_type = file
can_fail = True

[folder_names]
control_rep_2 = control_rep_2

[extension_regex]
regex = _L???_R?_???.fastq.gz
no_lane = _R?_???.fastq.gz
extension = .fastq.gz
read_one = _R1_001
read_two = _R2_001
"""

test_input = b"""[input_type]
paired_end = True

[project]
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'

[bcl2fastq]
cmd = bcl2fastq
input_cvs = file.csv
depends = None
dependency_type = prepare

[trimming_galore]
cmd = trim_galore
quality = 10
stringency = 8
gzip = True
depends = bcl2fastq
dependency_type = file

[align_bwa]
cmd = bwa mem
reference = /scratcha/sblab/portel01/reference_data/genomes/mus_musculus/mm9/Sequence/BWAIndex/genome.fa
M = True
t = 8
short_options = True
depends = trimming_galore
dependency_type = file

[mergebam]
cmd = samtools merge 
depends = align_bwa
dependency_type = folder

[filterduplicates]
cmd = /home/martin03/sw/picard/picard-2.8.2/picard-2.8.2.jar
whitelist = /scratcha/sblab/portel01/project/euni_5fc/annotations/cpgi/mm9-whitelist.bed
sam_include = 3
sam_exclude = 3840
depends = mergebam
dependency_type = folder

[quality]
cmd = fastqc
depends = trimming_galore
dependency_type = folder

[multiqc]
cmd = multiqc
depends = quality
dependency_type = project

[submit]
cmd = submit_report.py
depends = multiqc
dependency_type = project

[folder_names]
control_rep_2 = control_rep_2

[extension_regex]
regex = _L???_R?_???.fastq.gz
no_lane = _R?_???.fastq.gz
extension = .fastq.gz
read_one = _R1_001
read_two = _R2_001
"""

test_input_multidep = b"""[input_type]
paired_end = True

[project]
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'

[bcl2fastq]
cmd = bcl2fastq
input_cvs = file.csv
depends = None
dependency_type = prepare

[trimming_galore]
cmd = trim_galore
quality = 10
stringency = 8
gzip = True
depends = bcl2fastq
dependency_type = file

[align_bwa]
cmd = bwa mem
reference = /scratcha/sblab/portel01/reference_data/genomes/mus_musculus/mm9/Sequence/BWAIndex/genome.fa
M = True
t = 8
short_options = True
depends = trimming_galore
dependency_type = file

[mergebam]
cmd = samtools merge 
depends = align_bwa
dependency_type = folder

[filterduplicates]
cmd = /home/martin03/sw/picard/picard-2.8.2/picard-2.8.2.jar
whitelist = /scratcha/sblab/portel01/project/euni_5fc/annotations/cpgi/mm9-whitelist.bed
sam_include = 3
sam_exclude = 3840
depends = mergebam
dependency_type = folder

[quality]
cmd = fastqc
depends = trimming_galore
dependency_type = folder

[preseq]
# this should stay like that
cmd = preseq
depends = mergebam
dependency_type = folder
can_fail = True

[multiqc]
cmd = multiqc
depends = quality preseq
dependency_type = project

[submit]
cmd = submit_report.py
depends = multiqc
dependency_type = project

[folder_names]
control_rep_2 = control_rep_2

[extension_regex]
regex = _L???_R?_???.fastq.gz
no_lane = _R?_???.fastq.gz
extension = .fastq.gz
read_one = _R1_001
read_two = _R2_001
"""

test_input_remove = b"""[input_type]
paired_end = True

[project]
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'


[trimming_galore]
cmd = trim_galore
quality = 10
stringency = 8
gzip = True
depends = None
dependency_type = file
clean_up = True

[align_bwa]
cmd = bwa mem
reference = /scratcha/sblab/portel01/reference_data/genomes/mus_musculus/mm9/Sequence/BWAIndex/genome.fa
M = True
t = 8
short_options = True
depends = trimming_galore
dependency_type = file
[folder_names]
control_rep_2 = control_rep_2

[extension_regex]
regex = _L???_R?_???.fastq.gz
no_lane = _R?_???.fastq.gz
extension = .fastq.gz
read_one = _R1_001
read_two = _R2_001

"""

test_input_chipseq = b"""
[input_type]
paired_end = False

[project]
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'

[bcl2fastq]
cmd = bcl2fastq
input_cvs = file.csv
depends = None
dependency_type = prepare

[trimming_galore]
cmd = trim_galore
quality = 10
stringency = 8
gzip = True
depends = bcl2fastq
dependency_type = file

[align_bwa]
cmd = bwa mem
reference = /scratcha/sblab/portel01/reference_data/genomes/mus_musculus/mm9/Sequence/BWAIndex/genome.fa
M = True
t = 8
short_options = True
depends = trimming_galore
dependency_type = file

[mergebam]
cmd = samtools merge 
depends = align_bwa
dependency_type = folder

[filterduplicates]
cmd = /home/martin03/sw/picard/picard-2.8.2/picard-2.8.2.jar
whitelist = /scratcha/sblab/portel01/project/euni_5fc/annotations/cpgi/mm9-whitelist.bed
sam_include = 3
sam_exclude = 3840
depends = mergebam
dependency_type = folder

[chipseq]
cmd = macs2 callpeak
paired_control = no
control = _Input
enrich = _ChIP*
multifolder_postfix = _Input _ChIP*
depends = filterduplicates
dependency_type = multifolder

[quality]
cmd = fastqc
depends = trimming_galore
dependency_type = folder

[preseq]
# this should stay like that
cmd = preseq
depends = mergebam
dependency_type = folder
can_fail = True

[multiqc]
cmd = multiqc
depends = quality preseq chipseq
dependency_type = project

[folder_names]
control_rep_Input = control_rep_Input
control_rep_ChIP1 = control_rep_ChIP1
control_rep_ChIP2 = control_rep_ChIP2
control_rep2_Input = control_rep2_Input
control_rep2_ChIP2 = control_rep2_ChIP2

[extension_regex]
regex = _L???_R?_???.fastq.gz
no_lane = _R?_???.fastq.gz
extension = .fastq.gz
read_one = _R1_001
read_two = _R2_001

"""

test_input_se = b"""[input_type]
paired_end = False

[project]
project_name = Testing_jobrunner
run_name = first_run
report_name = 'Testing report'

[bcl2fastq]
cmd = bcl2fastq
input_cvs = file.csv
depends = None
dependency_type = prepare

[trimming_galore]
cmd = trim_galore
quality = 10
stringency = 8
gzip = True
depends = bcl2fastq
dependency_type = file

[align_bwa]
cmd = bwa mem
reference = /scratcha/sblab/portel01/reference_data/genomes/mus_musculus/mm9/Sequence/BWAIndex/genome.fa
M = True
t = 8
short_options = True
depends = trimming_galore
dependency_type = file

[mergebam]
cmd = samtools merge 
depends = align_bwa
dependency_type = folder

[filterduplicates]
cmd = /home/martin03/sw/picard/picard-2.8.2/picard-2.8.2.jar
whitelist = /scratcha/sblab/portel01/project/euni_5fc/annotations/cpgi/mm9-whitelist.bed
sam_include = 3
sam_exclude = 3840
depends = mergebam
dependency_type = folder

[folder_names]
C4P2 = C4P2

[extension_regex]
regex = _L???_R?_???.fastq.gz
no_lane = _R?_???.fastq.gz
extension = .fastq.gz
read_one = _R1_001
read_two = _R2_001
"""

example_folder = "control_rep_2"
example_files = ['control_rep_2_L001_R2_001.fastq.gz',
                 'control_rep_2_L001_R1_001.fastq.gz',
                 'control_rep_2_L002_R2_001.fastq.gz',
                 'control_rep_2_L002_R1_001.fastq.gz']

example_files_folders_chip = {"control_rep_Input":
                                  ["control_rep_Input_L001_R1_001.fastq.gz",
                                   "control_rep_Input_L001_R2_001.fastq.gz"],
                              "control_rep_ChIP1":
                                  ["control_rep_ChIP1_L001_R1_001.fastq.gz",
                                   "control_rep_ChIP1_L001_R2_001.fastq.gz"],
                              "control_rep_ChIP2":
                                  ["control_rep_ChIP2_L001_R1_001.fastq.gz",
                                   "control_rep_ChIP2_L001_R2_001.fastq.gz"],
                              "control_rep2_Input":
                                  ["control_rep2_Input_L001_R1_001.fastq.gz",
                                   "control_rep2_Input_L001_R2_001.fastq.gz"],
                              "control_rep2_ChIP2":
                                  ["control_rep2_ChIP2_L001_R1_001.fastq.gz",
                                   "control_rep2_ChIP2_L001_R2_001.fastq.gz"],
                              }

example_wrong_files = ['control_rep_2_L001_R2_001.fastq.gz',
                       'control_rep_2_L002_R2_001.fastq.gz',
                       'control_rep_2_L002_R1_001.fastq.gz']

example_regex = '_L???_R?_???.fastq.gz'

example_unique = {'control_rep_2_L001', 'control_rep_2_L002'}

fake_folders = ["trimming_control_rep_2"]

# Single end
example_folder_se = 'C4P2'
example_files_se = ['C4P2_L001_R1_001.fastq.gz',
                    'C4P2_L002_R1_001.fastq.gz',
                    'C4P2_L003_R1_001.fastq.gz']
example_unique_se = {'C4P2_L001',
                     'C4P2_L002',
                     'C4P2_L003'}

example_regex_se = '_L???_R?_???.fastq.gz'

fake_folders_se = ['trimming_C4P2']


def create_test_seq_files(files, folder=example_folder):
    cwd = os.getcwd()
    dir_name = os.path.join(cwd, folder)
    try:
        os.makedirs(dir_name)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    for f in files:
        open(dir_name + "/" + f, 'a').close()


def remove_test_files(folder=example_folder):
    cwd = os.getcwd()
    shutil.rmtree(cwd + "/" + folder)


def remove_runner_test_files(folders):
    cwd = os.getcwd()
    if os.path.isfile('cmd_log.txt'):
        os.remove('cmd_log.txt')
    # shutil.rmtree(cwd + "/" + example_folder)
    for fake in set(folders):
        if "multiqc_" in fake:
            shutil.rmtree(cwd + "/" + "multiqc_folder")
        elif "submit_" in fake:
            shutil.rmtree(cwd + "/" + "submit_folder")
        else:
            shutil.rmtree(cwd + "/" + fake)


@pytest.fixture()
def app_can_fail():
    # create a temporary directory to store the files during testing
    with tempfile.TemporaryDirectory() as temp_d:
        with tempfile.NamedTemporaryFile(dir='/tmp') as f_input:
            f_input.write(test_input_can_fail)
            f_input.flush()
            settings_override = {
                'LOG_DIRECTORY': temp_d,
                'TEST': True,
                'INPUT_FILE': f_input.name
            }

            # overwrite configuration
            for k, v in settings_override.items():
                setattr(BasicConfig, k, v)

            test_run = runner_factory(BasicConfig)

            create_test_seq_files(example_files)

            dict_return = {'runner': test_run,
                           'example_folder': example_folder,
                           'example_files': example_files,
                           'example_unique': example_unique,
                           'example_regex': example_regex}

            yield dict_return

            folders_remove = []
            for st, opt in test_run.run_options['stages'].items():
                if opt['dependency_type'] == "prepare":
                    folders_remove.append(f"{st}_prepare")
                else:
                    folders_remove.append(f"{st}_{example_folder}")

            remove_runner_test_files(folders_remove)


@pytest.fixture()
def app_memory():
    # create a temporary directory to store the files during testing
    with tempfile.TemporaryDirectory() as temp_d:
        with tempfile.NamedTemporaryFile(dir='/tmp') as f_input:
            f_input.write(test_input_memory_stage)
            f_input.flush()
            settings_override = {
                'LOG_DIRECTORY': temp_d,
                'TEST': True,
                'INPUT_FILE': f_input.name
            }

            # overwrite configuration
            for k, v in settings_override.items():
                setattr(BasicConfig, k, v)

            test_run = runner_factory(BasicConfig)

            create_test_seq_files(example_files)

            dict_return = {'runner': test_run,
                           'example_folder': example_folder,
                           'example_files': example_files,
                           'example_unique': example_unique,
                           'example_regex': example_regex}

            yield dict_return

            folders_remove = []
            for st, opt in test_run.run_options['stages'].items():
                if opt['dependency_type'] == "prepare":
                    folders_remove.append(f"{st}_prepare")
                else:
                    folders_remove.append(f"{st}_{example_folder}")

            remove_runner_test_files(folders_remove)


@pytest.fixture()
def app_se():
    # create a temporary directory to store the files during testing
    with tempfile.TemporaryDirectory() as temp_d:
        with tempfile.NamedTemporaryFile(dir='/tmp') as f_input:
            f_input.write(test_input_se)
            f_input.flush()
            settings_override = {
                'LOG_DIRECTORY': temp_d,
                'TEST': True,
                'INPUT_FILE': f_input.name
            }

            # overwrite configuration
            for k, v in settings_override.items():
                setattr(BasicConfig, k, v)

            test_run = runner_factory(BasicConfig)

            create_test_seq_files(example_files_se, folder=example_folder_se)

            dict_return = {'runner': test_run,
                           'example_folder': example_folder_se,
                           'example_files': example_files_se,
                           'example_unique': example_unique_se,
                           'example_regex': example_regex}

            yield dict_return

            remove_test_files(folder=example_folder_se)


@pytest.fixture()
def app():
    # create a temporary directory to store the files during testing
    with tempfile.TemporaryDirectory() as temp_d:
        with tempfile.NamedTemporaryFile(dir='/tmp') as f_input:
            f_input.write(test_input)
            f_input.flush()
            settings_override = {
                'LOG_DIRECTORY': temp_d,
                'TEST': True,
                'INPUT_FILE': f_input.name
            }

            # overwrite configuration
            for k, v in settings_override.items():
                setattr(BasicConfig, k, v)

            test_run = runner_factory(BasicConfig)

            create_test_seq_files(example_files)

            dict_return = {'runner': test_run,
                           'example_folder': example_folder,
                           'example_files': example_files,
                           'example_unique': example_unique,
                           'example_regex': example_regex}

            yield dict_return

            remove_test_files()


@pytest.fixture()
def runner_app_chipseq():
    # create a temporary directory to store the files during testing
    with tempfile.TemporaryDirectory() as temp_d:
        with tempfile.NamedTemporaryFile(dir='/tmp') as f_input:
            f_input.write(test_input_chipseq)
            f_input.flush()
            settings_override = {
                'LOG_DIRECTORY': temp_d,
                'TEST': True,
                'INPUT_FILE': f_input.name
            }

            # overwrite configuration
            for k, v in settings_override.items():
                setattr(BasicConfig, k, v)

            test_run = runner_factory(BasicConfig)

            for folder, files in example_files_folders_chip.items():
                create_test_seq_files(files, folder=folder)

            dict_return = {'runner': test_run, }

            yield dict_return

            folders_remove = []
            for st, opt in test_run.run_options['stages'].items():
                if opt['dependency_type'] == "prepare":
                    folders_remove.append(f"{st}_prepare")
                else:
                    for f in example_files_folders_chip:
                        if st == "multiqc":
                            folders_remove.append("multiqc_folder")
                        elif st != 'chipseq':
                            folders_remove.append(f"{st}_{f}")
                    if st == "chipseq":
                        folders_remove.extend(["chipseq_control_rep_tech1",
                                               "chipseq_control_rep2_tech1",
                                               "chipseq_control_rep_tech2"])

            folders_remove += list(example_files_folders_chip.keys())
            remove_runner_test_files(folders_remove)


@pytest.fixture()
def runner_app():
    # create a temporary directory to store the files during testing
    with tempfile.TemporaryDirectory() as temp_d:
        with tempfile.NamedTemporaryFile(dir='/tmp') as f_input:
            f_input.write(test_input_multidep)
            f_input.flush()
            settings_override = {
                'LOG_DIRECTORY': temp_d,
                'TEST': True,
                'INPUT_FILE': f_input.name
            }

            # overwrite configuration
            for k, v in settings_override.items():
                setattr(BasicConfig, k, v)

            test_run = runner_factory(BasicConfig)

            create_test_seq_files(example_files)

            dict_return = {'runner': test_run,
                           'example_folder': example_folder,
                           'example_files': example_files,
                           'example_unique': example_unique,
                           'example_regex': example_regex}

            yield dict_return

            folders_remove = []
            for st, opt in test_run.run_options['stages'].items():
                if opt['dependency_type'] == "prepare":
                    folders_remove.append(f"{st}_prepare")
                else:
                    folders_remove.append(f"{st}_{example_folder}")

            folders_remove.append(example_folder)
            remove_runner_test_files(folders_remove)


@pytest.fixture()
def runner_app_cleaner():
    # create a temporary directory to store the files during testing
    with tempfile.TemporaryDirectory() as temp_d:
        with tempfile.NamedTemporaryFile(dir='/tmp') as f_input:
            f_input.write(test_input_remove)
            f_input.flush()
            settings_override = {
                'LOG_DIRECTORY': temp_d,
                'TEST': True,
                'INPUT_FILE': f_input.name
            }

            # overwrite configuration
            for k, v in settings_override.items():
                setattr(BasicConfig, k, v)

            test_run = runner_factory(BasicConfig)

            create_test_seq_files(example_files)

            dict_return = {'runner': test_run,
                           'example_folder': example_folder,
                           'example_files': example_files,
                           'example_unique': example_unique,
                           'example_regex': example_regex}

            yield dict_return

            folders_remove = []
            for st, opt in test_run.run_options['stages'].items():
                if opt['dependency_type'] == "prepare":
                    folders_remove.append(f"{st}_prepare")
                else:
                    folders_remove.append(f"{st}_{example_folder}")

            folders_remove.append(example_folder)
            remove_runner_test_files(folders_remove)


@pytest.fixture()
def app_wrong():
    # create a temporary directory to store the files during testing
    with tempfile.TemporaryDirectory() as temp_d:
        with tempfile.NamedTemporaryFile(dir='/tmp') as f_input:
            f_input.write(test_input)
            f_input.flush()
            settings_override = {
                'LOG_DIRECTORY': temp_d,
                'TEST': True,
                'INPUT_FILE': f_input.name
            }

            # overwrite configuration
            for k, v in settings_override.items():
                setattr(BasicConfig, k, v)

            test_run = runner_factory(BasicConfig)

            create_test_seq_files(example_wrong_files)

            dict_return = {'runner': test_run,
                           'example_folder': example_folder,
                           'example_files': example_files,
                           'example_regex': example_regex}

            yield dict_return

            remove_test_files()


@pytest.fixture(scope='session')
def config_parser():
    # prepare config to test parsing
    with tempfile.NamedTemporaryFile(dir='/tmp', delete=False) as f_input:
        f_input.write(test_input)
        f_input.flush()
        config = configparser.ConfigParser()
        config.read(f_input.name)
        yield config


@pytest.fixture(scope='session')
def config_parser_multidep():
    # prepare config to test parsing
    with tempfile.NamedTemporaryFile(dir='/tmp', delete=False) as f_input:
        f_input.write(test_input_multidep)
        f_input.flush()
        config = configparser.ConfigParser()
        config.read(f_input.name)
        yield config


@pytest.fixture(scope='session')
def config_parser_chipseq():
    # prepare config to test parsing
    with tempfile.NamedTemporaryFile(dir='/tmp', delete=False) as f_input:
        f_input.write(test_input_chipseq)
        f_input.flush()
        config = configparser.ConfigParser()
        config.read(f_input.name)
        yield config


@pytest.fixture(scope='session')
def config_parser_can_fail():
    # prepare config to test parsing
    with tempfile.NamedTemporaryFile(dir='/tmp', delete=False) as f_input:
        f_input.write(test_input_can_fail)
        f_input.flush()
        config = configparser.ConfigParser()
        config.read(f_input.name)
        yield config


@pytest.fixture(scope='session')
def config_parser_memory():
    # prepare config to test parsing
    with tempfile.NamedTemporaryFile(dir='/tmp', delete=False) as f_input:
        f_input.write(test_input_memory_stage)
        f_input.flush()
        config = configparser.ConfigParser()
        config.read(f_input.name)
        yield config


@pytest.fixture(scope='session')
def config_parser_conflict():
    # prepare config to test parsing
    with tempfile.NamedTemporaryFile(dir='/tmp', delete=False) as f_input:
        f_input.write(test_input_conflict)
        f_input.flush()
        config = configparser.ConfigParser()
        config.read(f_input.name)
        yield config
