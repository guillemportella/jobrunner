#!/usr/bin/env python3
import argparse
import json
import sys
from argparse import RawDescriptionHelpFormatter
from html.parser import HTMLParser
from typing import Dict

import requests

__doc__ = """
Submit the reports to the 'rundown' API
"""

WEB_HOST = 'https://www-rg4shape.ch.cam.ac.uk'
API_QD_UPLOAD = '/api/qd_upload'
API_PROJECT_UPLOAD = '/api/upload'
API_PROJECT_LOGIN = '/auth/login'

DESC_MAIN = '''\n\
Submit project reports to the 'rundown' API.

Either supply and identification token or a combination
of user name and password.
'''
whowhen = 'Guillem Portella, v0.1, 08-2018'


class ParserInputTag(HTMLParser):
    """Very crude implementation, fit for purpose"""

    def __init__(self):
        HTMLParser.__init__(self)
        self.csrf_value = ""

    def handle_starttag(self, tag, attrs):
        if tag == "input":
            for attr in attrs:
                if "value" in attr[0]:
                    self.csrf_value = attr[1]


def parse_arguments() -> Dict:
    """Parse the arguments.

    rtype: arguments dictionary
    """
    parser = argparse. \
        ArgumentParser(description=DESC_MAIN,
                       epilog=whowhen,
                       formatter_class=RawDescriptionHelpFormatter, )
    subparsers = parser.add_subparsers(help='help for subcommand',
                                       dest='choose a subcommand.', )
    subparsers.required = True
    parser.add_argument(
        '-f',
        metavar='html_file', type=str,
        required=True,
        help='An html file that you want to upload')

    # this subparser does not have required option, so I assign it to _
    parser_quick = subparsers.add_parser('quick',
                                         help="Submit a nameless report for "
                                              "quick inspection. It won't be "
                                              "added to the database. ")
    parser_long = subparsers.add_parser('project',
                                        help="Submit a full report.")

    parser_long.add_argument(
        '-p',
        '--project_name',
        type=str,
        required=True,
        help="Project name. ")
    parser_long.add_argument(
        '-r',
        '--run_name',
        type=str,
        required=True,
        help="Run name. ")

    parser_long.add_argument(
        '-fname',
        '--file_name',
        type=str,
        required=True,
        help="File name, this is the name that you will see in the database.")

    parser.add_argument(
        '-web',
        metavar='web_address', type=str,
        required=False,
        default=WEB_HOST,
        help='The domain name of the web hosting the API.'
    )
    parser.add_argument(
        '-u',
        '--username',
        required=True,
        metavar='user', type=str,
        help='The user name.'
    )
    # this should be required only if -u is set, but don't know
    # how to tell argparse to do that.
    parser.add_argument(
        '-p',
        '--password',
        metavar='password', type=str,
        required=True,
        help='The user password.'
    )

    parser_long.set_defaults(func=submit_long_form)
    parser_quick.set_defaults(func=submit_qd_form)

    # I don't know how to implement this logic using argparse lib, so
    # do it by hand

    # parse args and call the right function
    args = parser.parse_args()
    dict_args = vars(args)
    if bool(dict_args['username']) ^ bool(dict_args['password']):
        print("Username (-u, --username) and password  (-p, --password) "
              "should be given together")
        sys.exit()
    args.func(dict_args)


def submit_qd_form(args=None) -> bool:
    """Submit a file to the server API"""
    qd_address = args['web'] + API_QD_UPLOAD
    login_address = args['web'] + API_PROJECT_LOGIN
    print(qd_address)
    filename = args['f']
    session = requests.session()
    pwd = args['password']
    username = args['username']

    # post to login to get credentials
    # get a session first
    session = requests.session()
    headers = {'Content-Type': "application/json", }

    form_id = json.dumps({'username': username,
                          'password': pwd})
    resp_login = session.request("POST", login_address, data=form_id,
                                 headers=headers, )
    cooks = resp_login.cookies.get_dict()

    # set header with CSRF token
    my_header = {'X-CSRF-TOKEN': resp_login.json()['access_csrf'], }

    # now that we have the credential we can use the upload endpoint
    try:
        file_payload = {'file': open(filename, 'rb')}
    except FileNotFoundError:
        print(f"Can not find {filename} file.")
        sys.exit()

    post_reply = session.post(qd_address,
                              cookies=cooks,
                              files=file_payload,
                              headers=my_header,
                              )

    try:
        post_reply.raise_for_status()
    except requests.exceptions.RequestException as e:
        print(f"{e}")
        return False

    # what if I got a redirect?? (should not happen) 
    if post_reply.status_code == 200:
        return True

    return False


def submit_long_form(args) -> bool:
    """Submit a file along with the project information"""
    """Submit a file to the server API"""
    login_address = args['web'] + API_PROJECT_LOGIN
    upload_address = args['web'] + API_PROJECT_UPLOAD
    filename = args['f']
    pwd = args['password']
    user_or_token = args['username']
    html_name = args['file_name']
    project_name = args['project_name']
    run_name = args['run_name']

    print(f"Uploading {filename}, prj {project_name}, run {run_name}, "
          f"html {html_name}")
    username = args['username']

    # post to login to get credentials
    # get a session first
    session = requests.session()
    headers = {'Content-Type': "application/json", }

    form_id = json.dumps({'username': username,
                          'password': pwd})
    resp_login = session.request("POST", login_address, data=form_id,
                                 headers=headers, )
    cooks = resp_login.cookies.get_dict()

    # set header with CSRF token
    my_header = {'X-CSRF-TOKEN': resp_login.json()['access_csrf'], }

    # now that we have the credential we can use the upload endpoint
    try:
        file_payload = {'file': open(filename, 'rb')}
    except FileNotFoundError:
        print(f"Can not find {filename} file.")
        sys.exit()

    data = {
        'html_name': html_name,
        'project_name': project_name,
        'run_name': run_name,
    }
    post_reply = session.post(upload_address,
                              cookies=cooks,
                              files=file_payload, data=data,
                              headers=my_header,
                              )
    print(post_reply.text)
    try:
        post_reply.raise_for_status()
    except requests.exceptions.RequestException as e:
        print(f"{e}")
        return False

    # what if I got a redirect?? (should not happen)
    if post_reply.status_code == 200:
        return True

    return False

    return True


if __name__ == "__main__":
    """Upload the forms."""
    sys.exit(parse_arguments())
