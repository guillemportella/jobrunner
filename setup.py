from setuptools import setup, find_packages

from job_runner import __version__

setup(
    name="job_runner",
    version=__version__,
    python_requires='>3.6',
    packages=find_packages(),
    include_package_data=True,
    scripts=['jr', 'submit_report.py'],

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=['pytest', 'python-dotenv', 'Pyro4', 'fyrd', 'parse',
                      'python-daemon', 'pid', 'colorama'],

    # metadata for upload to PyPI
    license="MIT",
    keywords="job runner",
    project_urls={
         "Source Code": "https://gitlab.com/guillemportella/jobrunner",
    }

)
