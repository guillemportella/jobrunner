# How to contribute

There are a number of ways you can contribute, some involve coding and some not. 
All contributions are welcome!

Non-code related (instrumented via issues)

1. Write better documentation
2. Test the app with your files

Code related (instrumented via a pull request)

3. Include new tests to the test suite
4. Add new pipelines
5. Refine existing pipelines
6. Improve failsafe, parsing, etc...


## Code related contribution: where to start 

The script that runs the job is `jr launch`, but this simply creates an object of 
class `Runner`, defined in `job_runner/runner.py`. This `Runner` object is 
instantiated by passing the run_options (parsed input file) and the 
configuration object (debug options, logging options, etc).

The `Runner` type object's main public function is `run()`, which locates all
the files required, sets up the execution plan, keeps track of the 
 job dependencies and submits the jobs. 
 
The execution plan is determined by the `DependencyTree` class. After this, 
`Runner.run()` iterates through
all the jobs that depend on *files*, then all the jobs that depend of *folder*,
and finally performs the task that depend on the whole *project*.

At each stage, we keep track of the job ids. Before a job needs to be 
executed we check the job ids that the stage/job depends on with get_ids. 
After the job has returned an queue id, or a list of them, add them to the 
instance using add_id().


## How to add new jobs

The functions required to run a particular stage are retrieved by the 
`dispatch_job` function (from `job_runner/planner/jobs.py`). To add a new
job type, follow this simple instructions:

In `job_runner/planner/jobs.py`, bellow the definition of `SetupJobs` class, 
define a new class inheriting from `SetupJobs`, make its constructor 
take `JobConfig` as a parameter, and initialize the parent class. 
Then define a method called `build_cmd` that must return your command 
line as a string. FYI,`JobConfig` is a namedtuple which stores the following, 

```python
class JobConfig(NamedTuple):
    """Store the info required to run the jobs."""
    stage_name: str # name of the stage/job
    run_options: Dict # all the fields from the input file are available here
    folder: str # name of the folder
    f_name: Union[str, List[str]] # name of the particular file, maybe empty
    dep_ids: List[id] # ids of the jobs we depend on
    father: Node # object pointing at the characteristics of the parent job
    test: bool # if we are in testing  mode, does not launch the job
    job_counter: int # for book keeping
    log_file: str # the name of the log file for a given job
```

and these are made available to you inside your new job class, (e.g. 
`self.stage_name`, or `self.folder`, etc.)

See examples source code for examples on how to customize a given job. For 
example, a new job type could be defined like this.
 
 ```python
 
class NewJob(SetupJob):
   """Prepare new job type"""

    # this bit of boiler plate must be defined
   def __init__(self, config: JobConfig = None):
       super().__init__(config)

   # defined build_cmd() method. See JobConfig namedtuple for the type 
   # parameters you can access.
   def build_cmd(self) -> str:
       stage_options = self.run_options['stages'][self.stage_name]
       cmd = stage_options['cmd']
       extension = self.run_options['regex']['extension']
       read_1 = self.run_options['regex']['read_one']
       read_2 = self.run_options['regex']['read_two']
       out_folder = f"{self.stage_name}_{self.folder}"
       keyword_options = self.kw_options()
       
       cmd = f"{cmd} {keyword_options}" \
             f"-o {out_folder} " \
             f"--paired {self.folder}/{self.f_name}{read_1}{extension}" \
             f" {self.folder}/{self.f_name}{read_2}{extension} " \
             f"{keyword_options}"
       return cmd
```

Add your `NewJob` to the `registered_jobs` dictionary, with the key being 
the name you'd give to this stage in the input file. E.g., 

```python
# at the bottom of `job_runner/planer/jobs.py
registered_jobs = {
    'trimming': Trimmer,
    'align': Aligner,
    #[...]
    'new_thing': NewJob,
}
```

Finally, if your `NewJob` requires certain input options, add these requirement
in `job_runner/parse/parser.py` function `parse_input_file()`. See the other
jobs for examples on how this is done. If you don't, only  `cmd`, `depends` and
`dependency_type` will be required.

*Please make sure that your pull request passes the existing test, and add
tests to make sure your implementation works.* 
Refer to the [README.md](./README.md) to find out how to run the tests. 

#### Some technical background on how `job_runner/planner/jobs.py` works.

In this module we implement the definitions of the jobs and a function to 
launch them. The overview of the class inheritance is:

Submitter --> SetupJob --> Filter, Aligner, etc...

There are two main helper classes, Submitter and SetupJob.

Submitter provides a method to send jobs to the cluster and to create the
output folder. It adds a list of dependencies, based on queue job id, such
that a job won't start until all its dependencies have finished.

SetupJob inherits from Submitter, and stores the variables that are
needed to jobs. SetupJob is intended to be inherited by classes defining
actual jobs, and it enforces that these inherited classes implement a
build_cmd() method, which must return a string with the cmdline to be
executed in the cluster.

A job class must take as argument a JobConfig named tuple with all the
required options to run the job. Ultimately, when any job class has been
instantiated, one can execute the run() method to launch the job to
the queuing system. We only support SLURM by now, but we could add others
if we add, e.g., `fyrd` as a dependency.

For everything to work with the rest of the package, we must register
any job class in the registered_jobs dictionary, using the intended
job name as key and the class as value. N.B. the job name is the one used in
the input file, between square brackets.

Finally, `dispatch_job()` receives the job name/stage and a `JobConfig` 
variable, launches the job (if we are not in testing mode), and returns the 
job_id of that job.

#### Some technical background on how the dependencies are managed

The functionality to resolve the job dependencies in in 
`job_runner/planner/job_planner.py`.

###### Node, DependencyTree:

Classes to plan the execution order of the jobs to be performed based on
the dependencies they have. The DependencyTree get_plan method reads
in the dictionary of jobs/stages, builds a dependency tree and resolves
the path of execution in order to satisfy the dependencies. It returns a
dictionary with three keys: file, folder or project. In each we list the
 order of the jobs that need to be executed.

The first job to be executed must depend on None, and it is the root of the
tree. We establish the order of executions by doing a depth-first search across
the tree.

###### StoreIds:

Keep track of the job ids of each stage/job. Before a job needs to be executed
check the job ids your stage/job depends on with get_ids. After the job
has returned an queue id, or a list of them, add them to the instance using
add_id().
Internally, the class stores/retrieves the ids in the right dictionary based
on what the arguments that are passed. A job could depend on the file, folder
and the father (i.e. stage/job that it depends on). If file is None, it means
that it does not depend on the file, if both file and folder are None it means
that it only depends on the father.


## Adding new tests

Tests are fundamental if we want to make sure the applications does what is meant to do. 
There mainly two types of tests that we should write:

    - Unit tests -> test a simple function, eg. read_file(fnamel) actually reads the file.
    - Integration tests -> a bunch of "complex" steps work well together. Eg. submit a job to the cluster.

Tests can be found in the `tests` folder, and are written for `pytest`. In `conftest.py` we should 
keep the collection of *fixtures* that allow integration tests to run. For example, it will set up
the directories and files to test a given bit of functionality, and reset everything back to the original
state once the tests have finished. In `pytest`, you can simply call these *fixtures* to start testing
a given feature.   
