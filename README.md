
# Automating basic pipelines in next-generation sequencing 

Automate the submission to a `SLURM` cluster of the 
most common first steps in analysing genomic data:

- Adapter trimming
- Alignment
- Merging multiple lanes
- Filtering of duplicates 
- Quality control 

Please head over to the [documentation](https://guillemportella.gitlab.io/jobrunner/index.html)
 to get started. It is much more easy to browse than this readme.
 

![jr_demo](assets/show_runner.gif)

## Install 

It is a `python3` program only, so make sure that you are not using legacy
versions. The same applies to `pip`, must be the version that corresponds
to `python3`.

I use, and would recommend using, `conda` to manage your `python` versions, to make 
sure you are using the right environment. 

Install for your user like, 

```
git clone https://gitlab.com/guillemportella/jobrunner.git
cd jobrunner
pip install . --user
```

Then executable `jr` should be in your `PATH` and 
will execute the program. If you have installed it correctly, 
it should be in the path. Test if so by issuing, 

```bash
jr -h 
``` 

## Documentation 

Please head over to [https://guillemportella.gitlab.io/jobrunner](https://guillemportella.gitlab.io/jobrunner/index.html)
for the complete documentation. It contains all you need to know to run the 
programs.


## Contributing and adding new job types

Check the [contributing](./CONTRIBUTING.md) section, 
and the [documentation](https://guillemportella.gitlab.io/jobrunner/index.html),
specifically [this](https://guillemportella.gitlab.io/jobrunner/adding_jobs.html).
